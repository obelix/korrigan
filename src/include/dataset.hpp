////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * dataset.hpp
 *
 *  Created on: 23 sept. 2019
 *      Author: more
 */

#ifndef SRC_INCLUDE_DATASET_HPP_
#define SRC_INCLUDE_DATASET_HPP_

#include <boost/algorithm/string.hpp>

#include <gdal_priv.h>
#include "ogr_spatialref.h"
#include "ogrsf_frmts.h"

//#include "triskeleBase.hpp"
//#include "IImage.hpp"

using namespace std;

typedef uint16_t	PixelT;

// ======================================================================

OGRSpatialReference		gdalDataset2SpatialRef (GDALDataset *gdalDataset);

// ======================================================================

// point (=absolute position) in pixel units
struct PointPixel {
  int x, y;
  PointPixel (const int x=0, const int y=0);
};

// size (=relative position) in pixel units
struct SizePixel {
  int width, height;
  SizePixel (const int width=0, const int height=0);
};

// zone (=2 points) in pixel units
struct ZonePixel {
  int x1, y1, x2, y2;
  ZonePixel (const int x1=0, const int y1=0, const int x2=0, const int y2=0);
  ZonePixel (const SizePixel sizePixel);
  int width();
  int height();
};

// point (=absolute position) in geographical units
struct PointGeo {
	double x, y;
	PointGeo (const double x=0, const double y=0);
};

// size (=relative position) in geographical units
struct SizeGeo {
	double width, height;
	SizeGeo (const double width=0, const double height=0);
};

// zone (=2 points) in geographical units
struct ZoneGeo {
  double x1, y1, x2, y2;
  ZoneGeo (const double x1=0, const double y1=0, const double x2=0, const double y2=0);
  double width();
  double height();
};

// ======================================================================

class CRS {
	private:
		GDALDataset					*gdalDataset;
		string							projectionRef;
		vector<double>			geoTransform;
		OGRSpatialReference spatialRef;

//		OGRSpatialReference	GetSpatialRef ();

	public:
								CRS (GDALDataset *gdalDataset);

		// conversion from pixel coordinates to geographical coordinates
		PointGeo		pixelToGeo (PointPixel pointPixel);
		SizeGeo			pixelToGeo (SizePixel sizePixel);
		ZoneGeo			pixelToGeo (ZonePixel zonePixel);

		// conversion from geographical coordinates to pixel coordinates
		PointPixel	geoToPixel (PointGeo pointGeo);
		SizePixel		geoToPixel (SizeGeo sizeGeo);
		ZonePixel		geoToPixel (ZoneGeo zoneGeo);

		// CRS information
		int					getEPSG ();
		double			getNoDataValue (int band=0);
//		bool				containNoData (int band, ZonePixel zonePixel);
};

//// ======================================================================
//
//class Image {
//	private:
//		string					path;
//		GDALDataset			*gdalDataset;
//		vector<PixelT>	pixels;
//		SizePixel				size;
//		int							bandCount;
//		double					noDataValue;
//
//	public:
//									Image (string path);
//	void 						readInfo ();
//	void						readBand(int band);
//
//};
//
// ======================================================================

class Dataset {
	private:
		// image/patch/ground truth files and folders

		// image features
		string					imageDir;
		string					imageFile;
		SizePixel				imageSizePixel;
		int							imageBandcount;
		GDALDataset			*gdalImageDataset;
    ZoneGeo					imageZoneGeo;
    int							imageEPSG;
    double					noDataValue;

		// ground truth features
		string					gtDir;
		string					gtFile;
		SizePixel				gtSizePixel;
		int							gtBandcount;
		GDALDataset			*gdalGtDataset;
    ZoneGeo					gtZoneGeo;
    int							gtEPSG;

		// patches features
		string					patchDir;
		string					patchFilePattern;
		SizePixel				patchSizePixel;
		SizeGeo					patchSizeGeo;
		float						patchOverlap;
		string					patchCoord;

	private:
		//	void getImDimensionsUsingBash();
		void 					getImageAndGtInfo ();
		string				getPatchFile (int band, string outputPrefixStr, int indX, int indY, ZonePixel patchZonePixel, int GTClass, float maintclassTh);
		void 					cropImageUsingTriskele (string inputPath, string outputPath, int band, ZonePixel patchZonePixel);
		void 					cropImageUsingTriskele (string inputPath, string outputPath, int band, ZoneGeo patchZoneGeo, GDALDataset* gdalDataset);

		template<typename PixelT>
    void					readPixels (string path, int band);

		vector<float>	getNormHistogram (string path, int band);
		int						getMainClass (string path, int band, float mainclassTh);
		string				classIndexToClassname (int classIndex);

	public:
									Dataset();
		void 					Init (string imageDir, string imageFile,
												string gtDir="", string gtFile="");
		void					CreatePatches (string patchDir, SizeGeo patchSizeGeo, float patchOverlap, float mainclassTh);
		void					CreatePatches (string patchDir, SizePixel patchSizePixel, float patchOverlap, float mainclassTh);
		void					CreatePatchesPreviews (string patchDir, string patchPreviewDir);
};

// ======================================================================

ZoneGeo projectZoneGeo(ZoneGeo sourceZoneGeo, GDALDataset *gdalSourceDataset, GDALDataset *gdalTargetDataset);

// ======================================================================

#endif /* SRC_INCLUDE_DATASET_HPP_ */
