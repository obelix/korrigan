////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_PatternSpectra_hpp
#define _Obelix_Triskele_PatternSpectra_hpp

/**
 * F. Raimbault
 * november 2019
 *
 * A Pattern Spectra contents and it associated operations.
 * A PS is computed from a full image, or an image patch
 * A PS models a two dimensional histogram, relating two variables.
 *
 * It has the following attributes:
 * - a linear vector of float : bins
 *   managed has a two dimensional array (H[i][j]= V[i*dim2+j])
 *   containing the  values falling into each interval (bucket or bin) along the first and the second dimension
 * - two dimensions : dim1 and dim2
 *   dim1 is the number of bins of the first variable
 *   dim2 is the number of bins of the second variable
 *   One-dimensional histogram has dim2 = 1
 * - the range of each variable value : range1 and range2
 *   range1 is the range of values of the first variable
 *   range2 is the range of values of the second variable
 *   so the width of the bins of the first (second) interval is range1/dim1 (range2/dim2)
 * - the coordinates of the image patch : x_pos and y_pos
 *   give the position inside the source image from which the PS has been calculated
 * - the size of the image patch : x_size and y_size
 *   give the size in pixels of the image patch inside the source image
 * - the PS filename : ps_filename
 *   the HDF5 file containing this PS, and also all the PS computed from the same image
 * - the image filename : image_filename
 *   the HDF5 file containing the image from which the PS has been computed
 *
 *   Available operations:
 *
 *   - fill_from_matrix(mat[])
 *     fills the bins from the matrix
 *     return 0 on success, otherwise an error code
 *
 *   - add_count(int count, int int v1,int v2)
 *     Add count value to the bins[v1][v2]
 *
 *   - assign_count(int count, int v1, inv2)
 *     set the bins[v1][v2] to count value
 *
 *   - get_count(int v1,v2)
 *     get the bins[v1][v2] value
 *
 *   - compare_to(ps)
 *     compare two PS
 *     returns the computed value or -1 if the dimensions of the PS are not the same
 *
 *   - open_read_h5(filename)
 *     open an existing HDF5 file to read full image PS or sub image PS
 *     return 0 on success, otherwise an error code
 *
 *   - open_write_h5(filename)
 *     open an existing HDF5 file to write full image PS or sub image PS
 *     return 0 on success, otherwise an error code
 *
 *   - close_h5()
 *     open an already opened HDF5 file
 *     return 0 on success, otherwise an error code
 *
 *   - load_from_h5()
 *     retrieve the contents of a PS from a already opened HDF5 file in which it has been saved with all the computed PS of the same source image
 *     the PS bins are read from a one-dimensional dataset named "PS" located into a HDF5 group "X=x_pos"/"Y=y_pos"
 *     return 0 on success, otherwise an error code
 *
 *   - store_to_h5()
 *     add the contents of a PS to an already opened HDF5 file which contains all the computed PS of the same source image
 *     PS having position in the image is saved on a HDF5 group "X=x_pos"/"Y=y_pos" into a one-dimensional dataset named "PS"
 *     return 0 on success, otherwise an error code
 *
 */

#include <vector>
#include <string>
#include "H5Cpp.h"

namespace obelix {
  namespace triskele {

    class PatternSpectra {

    public:
      /**
       * Build an empty and initialized two-dimensional histogram (two variables) for an image patch from an image source
       * dim1, dim2 : first and second dimension of the two-dimensional histogram (dim2= 1 for one-dimensional histogram)
       * range1,range2 : range of the variable values
       * xposx,ypos : positions of the image patch inside the source image
       * xsize,ysize : size in pixels of the image patch
       * filename : HDF5 filename of the source image
       */
      PatternSpectra(int dim1, int dim2, double range1, double range2, int xpos,
		     int ypos, int xsize, int ysize, std::string filename);
      /**
       * Fill in the PS with a matrix given as a one-dimensional array
       * matrix : the matrix from which the bin values are read
       * return 0 on success, otherwise an error code
       */
      int
      fill_from_matrix(uint32_t matrix[]);
      /*
       *  Add value cnt to the bins[v1][v2]
       *  return the resulting count value in bins[v1][v2]
       *
       */
      inline uint32_t
      add_count(uint32_t cnt, int v1,int v2){
	bins[v1*dim_2+v2]+=cnt;
	return bins[v1*dim_2+v2];
      }
      /*
       *  Set the count value of bins[v1][v2] to cnt
       *  return the resulting count value in bins[v1][v2]
       */
      inline uint32_t
      assign_count(uint32_t cnt, int v1, int v2){
	bins[v1*dim_2+v2]=cnt;
	return bins[v1*dim_2+v2];
      }
      /*
       * Get the count value of bins[v1][2]
       */
      inline uint32_t
      get_count(int v1,int v2){
	return bins[v1*dim_2+v2];
      }
      /**
       * Compare the PS with an another PS
       * ps : the other PS to compare ; dimensions should be the same
       * return -1 if the dimensions are different, otherwise the computed distance
       */
      double
      compareTo(PatternSpectra & ps);
      /**
       * Open a HDF5 file for reading
       * filename : the HDF5 file name containing all the PS of the same source image
       * return 0 on success, otherwise an error code
       */
      int
      open_read_h5(const std::string filename);
      /**
       * Open a HDF5 file for writing
       * filename : the HDF5 file name containing all the PS of the same source image
       * return 0 on success, otherwise an error code
       */
      int
      open_write_h5(const std::string filename);
      /**
       * Close a previously open HDF5 file by open_read_hdf5() or open_write_hdf5()
       * return 0 on success, otherwise an error code
       */
      int
      close_h5();
      /**
       * Fill in the PS with a previously saved content into an HDF5 file (see store_to_hdf5).
       * The file must has been opened with open_read_hdf5()
       * return 0 on success, otherwise an error code
       */
      int
      load_from_h5();
      /**
       * Save the PS into an HDF5 file.
       * The file must has been opened with open_write_hdf5()
       * return 0 on success, otherwise an error code
       */
      int
      store_to_h5();

      virtual ~PatternSpectra() = default;

    private:
      std::vector<uint32_t> bins; // histogram contents
      int dim_1, dim_2; // first and second dimensions
      double range_1, range_2; // range of values in each dimension
      int x_pos, y_pos; // position of the image patch H5::H5File _hdf5_file (0,0 for the full image)
      int x_size, y_size; // size of the image patch (or the width and the height of the full image)
      std::string image_filename; // filename of the source image
      H5std_string ps_h5filename; // filename of the HDF5 file containing the PS
      H5::H5File ps_h5file; // the current HDF5 file descriptor
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_PatternSpectra_hpp
