////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Korrigan_KorriganStats_hpp
#define _Obelix_Korrigan_KorriganStats_hpp

#include <iostream>
#include <boost/chrono.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

namespace obelix {
  namespace korrigan {

    using namespace std;
    using namespace boost::chrono;
    namespace ba = boost::accumulators;
    typedef ba::accumulator_set<double, ba::stats<ba::tag::sum,
						  ba::tag::count,
						  ba::tag::mean,
						  ba::tag::min,
						  ba::tag::max> > KorriganStatsDouble;

    // ================================================================================
    enum KTimeType {
		    PScompute,
		    PSfilter,
		    HDF5save,
		    HDF5load,

		    KTimeTypeCard
    };

    // ================================================================================
    class KorriganStats {
      public :

      static KorriganStats global;
      static const string timeTypeLabels[];

      KorriganStats ();

      void reset ();
      inline void setStart ();
      inline void addTime (const KTimeType &timeType);
      inline const KorriganStatsDouble &getTimeStats (const KTimeType &timeType);

      // nice ostream
      struct CPrintTime {
	const KorriganStats &korriganStats;
	CPrintTime (const KorriganStats &korriganStats);
	ostream &print (ostream &out) const;
      };
      CPrintTime printTime () const;
      friend ostream &operator << (ostream& out, const CPrintTime &cpt) { return cpt.print (out); }

    private :
      high_resolution_clock::time_point start;
      vector<KorriganStatsDouble> timeStats;
    };

    // ================================================================================
#include "KorriganStats.tpp"
  } // korrigan
} // obelix

#endif // _Obelix_Korrigan_KorriganStats_hpp
