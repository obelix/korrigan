////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * Files.hpp
 *
 *  Created on: 11 sept. 2019
 *      Author: more
 */

#ifndef INCLUDE_FILES_HPP_
#define INCLUDE_FILES_HPP_

#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
namespace fs = boost::filesystem;


// ----------------------------------------------------------------------

class Files {
private:
	void	printPathList (vector<fs::path> pathList);

public:
	void	createInputPathList (string inputDir, string inputExt="tif");
	void	createOutputPathList (string outputDir, string outputExt="ps");
	void	printInputPathList ();
	void	printOutputPathList ();
	void	writeOutputPathList ();

private:
	vector<fs::path>		inputPathList;
	vector<fs::path>		outputPathList;
};

// ----------------------------------------------------------------------

#endif /* INCLUDE_FILES_HPP_ */

