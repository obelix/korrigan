**Python demo code for patch retrieval using sliding window.**

You need to install [siamxt](https://github.com/rmsouza01/siamxt) package to run the code.


- Build Median tree on Query and Search window
- Extract 2D pattern spectra based on Area and Rectangularity attributes
- Compute chi-square distance between descriptors
