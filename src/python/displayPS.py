#! /usr/bin/python3

## to run type in terminal:
## ./displayPS.py

## if needed, install h5py package before:
## pip3 install h5py

# ----------------------------------------
#Korrigan.hpp:
# define MAX_AREA_BIN 100000000
#
#Korrigan.cpp:
# const DimBin obelix::korrigan::binRectCount (30);
# const DimBin obelix::korrigan::binAreaCount (30);
#
# vector<double>
# obelix::korrigan::logspace (const double &a, const double &b, const int &k) {
#   vector<double> logspace;
#   logspace.reserve (k);
#   double exp_scale = (std::log (b) - std::log (a)) / (k - 1);
#   for (int i = 0; i < k; ++i)
#     logspace.push_back (pow (2, std::log (a) + i * exp_scale));
#   return logspace;
# }
#
#fullPS.cpp:
# vector<double> rectScale = logspace (1./MAX_AREA_BIN, 1., binRectCount);
# vector<double> areaScale = logspace (1, MAX_AREA_BIN, binAreaCount);

# ----------------------------------------

import sys
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.image import NonUniformImage
from matplotlib.ticker import ScalarFormatter
from matplotlib import cm

# ----------------------------------------

rectLinear = False
areaLinear = True

if rectLinear:
	print(" -> using linear scale for rectangularity")
else:
	print(" -> using log scale for rectangularity")


# ----------------------------------------

if len(sys.argv)<2:
	print ('usage:   displayPS.py <filename>.kor or displayPS <filename>.patch')
	sys.exit(0)

pspath = sys.argv[1]
psdir = os.path.dirname(os.path.realpath(pspath))
psfile = os.path.basename(os.path.realpath(pspath))
psext = os.path.splitext(psfile)[1]

# ----------------------------------------

MAX_AREA_BIN = 100000000

# on x-axis

if rectLinear:
	binRectCount = 30
	binRectMin = 0.
	binRectMax = 1.
else:
	binRectCount = 30
	binRectMin = 1./MAX_AREA_BIN
	binRectMax = 1.

# on y-axis
if areaLinear:
        binAreaCount = 30
        binAreaMin = 1
        binAreaMax = MAX_AREA_BIN
else:
        binAreaCount = 30
        binAreaMin = 1
        binAreaMax = MAX_AREA_BIN

# ----------------------------------------

if rectLinear:
        axisRectMin = binRectMin
        axisRectMax = binRectMax
else:
        axisRectMin = np.log10(binRectMin)
        axisRectMax = np.log10(binRectMax)

if areaLinear:
        axisAreaMin = np.log10(binAreaMin)
        axisAreaMax = np.log10(binAreaMax)
else:
        axisAreaMin = binAreaMin
        axisAreaMax = binAreaMax

# ----------------------------------------

import h5py
import numpy as np

f = h5py.File(pspath, 'r')

# case 1) read variable PS in kor file
if psext=='.kor':
	print('Display Pattern Spectra (from *.kor file, HDF5 format)')
	print()
	subtitle='(full PS)'
	PS1D = np.array(f['PS'][:])

# case 2) read variable  in patch file and regenerate variable PS
if psext=='.patch':
	print('Regenerate and display Pattern Spectra (from *.patch file, HDF5 format)')
	print()
	subtitle='(sub PS)'
	patchPSBinIdx = np.array(f['patchPSBinIdx'][:])
	patchPSBinVal = np.array(f['patchPSBinVal'][:])
	nPatchBin = patchPSBinIdx.shape[0]
	# deduce PS array
	PS1D = np.zeros(binRectCount*binAreaCount)
	for k in range(nPatchBin):
		PS1D [ patchPSBinIdx[k] ] += patchPSBinVal[k]

# test to check row and column order and directions
# binRectCount = 20
# PS1D = np.arange(0,binAreaCount*binRectCount)
# PS1D = np.where(PS1D < 30, PS1D, 30)

# PSnelem=PS1D.size
# PSXsize=int(np.round(np.sqrt(PSnelem)))
# PSYsize=int(np.round(PSnelem/PSXsize))

# PS 1D -> PS 2D
PSTrans = PS1D.reshape(binRectCount, binAreaCount)
# C++ is column-major, but numpy is row-major, so transpose
PS = np.transpose(PSTrans)

print('pspath:')
print(pspath)
print('PS:')
print(' type:', PS.dtype)
print(' dimensions): ', PS1D.shape, ' -> ', PS.shape)
print()

if rectLinear:
	binRect = np.linspace(binRectMin, binRectMax-(binRectMax-binRectMin)/binRectCount, binRectCount)
else:
	binRect = np.logspace(np.log10(binRectMin), np.log10(binRectMax), binRectCount)
binArea = np.logspace(np.log10(binAreaMin), np.log10(binAreaMax), binAreaCount)

print('binRect:\n', binRect)
print()
print('binArea:\n', binArea)
print()
#print('PS:\n', PS)
print(' ', PS1D[0:4])
print()
print(' ', PS[0:4,0])
print()
#PS[:,0]=1e8

# ----------------------------------------
print("PS values:")
print()
for y in range(binAreaCount):
	for x in range(binRectCount):
		k = x + binRectCount*y
#		print ('[',x,';',y,']:',PS[x,y], end='')
#		print ('[',k,']:',PS1D[k], end='')
		print (PS1D[k], ' ', end='')
	print()
print()

# ----------------------------------------

cmap = cm.YlOrRd
cmap.set_bad(color='gray')
PSTransMask = np.ma.masked_where(PSTrans == 0, PSTrans)

plt.imshow(PSTransMask, origin='lower', cmap=cmap)

# plt.grid(True)

#plt.title(psfile+'\n'+subtitle)
plt.title(psfile)
plt.xlabel('Compactness')
plt.ylabel('Area')
ax = plt.gca()

if rectLinear:
	tickLabelsRect = ['0.', '0.2', '0.4', '0.6', '0.8', '1.0']
	tickValuesRect = list(np.float_(tickLabelsRect))
	ticksPosRect = (tickValuesRect-min(tickValuesRect)) / (max(tickValuesRect)-min(tickValuesRect))  * (binRectCount-1)
else:
	tickLabelsRect = ['1e-8', '1e-6', '1e-4', '1e-2', '1e0']
	tickValuesRect = list(np.float_(tickLabelsRect))
	tickLogvaluesRect = np.log10(tickValuesRect)
	ticksPosRect = (tickLogvaluesRect-min(tickLogvaluesRect)) / (max(tickLogvaluesRect)-min(tickLogvaluesRect))  * (binRectCount-1)

### ax.ticklabel_format(style='sci',axis='y')
ax.set_xticklabels(tickLabelsRect)
ax.set_xticks(ticksPosRect)

if areaLinear:
        tickLabelsArea = ['0.', '0.2', '0.4', '0.6', '0.8', '1.0']
        tickValuesArea = list(np.float_(tickLabelsArea))
        ticksPosArea = (tickValuesArea-min(tickValuesArea)) / (max(tickValuesArea)-min(tickValuesArea)) * (binAreaCount-1)
else:
        tickLabelsArea = ['1e0', '1e2', '1e4', '1e6', '1e8']
        tickValuesArea = list(np.float_(tickLabelsArea))
        tickLogvaluesArea = np.log10(tickValuesArea)
        ticksPosArea = (tickLogvaluesArea-min(tickLogvaluesArea)) / (max(tickLogvaluesArea)-min(tickLogvaluesArea))  * (binAreaCount-1)

### ax.ticklabel_format(style='sci',axis='x')
ax.set_yticklabels(tickLabelsArea)
ax.set_yticks(ticksPosArea)

plt.colorbar()

plt.show()

# ----------------------------------------

## fig = plt.figure()
## ax = plt.gca()

## im = NonUniformImage(ax, extent=(binRectMin, binRectMax, binAreaMin, binAreaMax))
## im.set_data(binRect, binArea, PS)
## ax.images.append(im)
## ax.set_xlim(binRectMin, binRectMax)
## ax.set_ylim(binAreaMin, binAreaMax)

## plt.show()

# ----------------------------------------

