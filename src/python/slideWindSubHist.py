# Undirectly compute sub-histogram for each image patches

import siamxt
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from scipy import stats
from operator import itemgetter
from scipy.stats import chisquare
import time

def rgb2gray(imgRGB):
    r, g, b = imgRGB[:,:,0], imgRGB[:,:,1], imgRGB[:,:,2]
    imgGray = np.uint16(0.2989 * r + 0.5870 * g + 0.1140 * b)
    negImg = np.max(imgGray) - imgGray
    medImg = np.uint16(np.abs(imgGray-np.median(imgGray)))
    return imgGray

def treeNodes(imgRGB):
    # node array of the whole image
    imgGray = rgb2gray(imgRGB)
    Bc = np.ones((3,3), dtype=bool)         # connectivity
    mxt = siamxt.MaxTreeAlpha(imgGray,Bc)   # Max tree
    nodes = mxt.node_array
    return nodes

def patchNodes(imgNodes, x1, x2, y1, y2):
    # node array of the sub image
    # xmin = mxt.node_array[6,:]
    # xmax = mxt.node_array[7,:]
    # ymin = mxt.node_array[9,:]
    # ymax = mxt.node_array[10,:]

    subNodes = imgNodes[:,(imgNodes[6,:]>x1) & (imgNodes[7,:]<x2) & (imgNodes[9,:]>y1) & (imgNodes[10,:]<y2)]
    return subNodes

def nodeHist(subNodes, allNodes):
    nodeParent = subNodes[0, :]
    nodeLevel = subNodes[2, :]
    nodeParLevel = allNodes[2, nodeParent]
    nodeDifLevel = nodeLevel - nodeParLevel

    nodeArea = subNodes[3, :]
    nodeVolume = nodeDifLevel * nodeArea
    # print("nodeArea", np.min(nodeArea), np.max(nodeArea))
    # print("nodeVolume", np.min(nodeVolume), np.max(nodeVolume))

    xmin,xmax = subNodes[6,:], subNodes[7,:] + 1
    ymin,ymax = subNodes[9,:], subNodes[10,:] + 1
    nodeRR = 1.0 * nodeArea / ((xmax - xmin) * (ymax - ymin))

    nBin = 30

    minArea = np.min(allNodes[3,:])
    maxArea = np.max(allNodes[3,:])
    log_edges_area = np.logspace(np.log10(minArea), np.log10(maxArea), num=nBin)
    lin_edges_area = np.linspace(minArea, maxArea, num=nBin)

    minRR = 1e-10
    maxRR = 1
    log_edges_rr = np.logspace(np.log10(minRR), np.log10(maxRR), num=nBin)
    lin_edges_rr = np.linspace(minRR, maxRR, num=nBin)

    hist1d1, edges1, binIdxs1d1 = stats.binned_statistic(nodeArea, nodeVolume, statistic='sum', bins=log_edges_area)
    hist1d2, edges2, binIdxs1d2 = stats.binned_statistic(nodeRR, nodeVolume, statistic='sum', bins=log_edges_rr)

    ret2 = stats.binned_statistic_2d(nodeArea, nodeRR, nodeVolume, statistic='sum', bins=[log_edges_area, log_edges_rr],
                                     expand_binnumbers=True)
    hist2d = ret2.statistic
    hist2d1 = hist2d.flatten()
    binIdxs2d = ret2.binnumber
    binIdxs2dlin = (nBin-1)*(binIdxs2d[0,:]-1) + (binIdxs2d[1,:]-1)
    #print("binIdxs2",binIdxs2.shape)

    return hist1d1, binIdxs1d1

def Distance(x,y):
    statistic = 0.5*np.sum((x-y)**2/(x+y+1e-6))
    #statistic, p = chisquare(x,y+1e-6)
    #statistic, p= stats.ks_2samp(x,y)
    #dist = stats.wasserstein_distance(x,y)
    return statistic

path = "/home/mirmahbo/Documents/Projects/cnes/codes/"
img_png_q = 'image01.png'
img_png_s = 'image11.png'
img_tif_q = 'IMG_L2A_TOA_20190214_a20190205T102913_pPHR1A_mMS.TIF'


queryImage = io.imread(path + img_png_q)
searchImage = io.imread(path + img_png_s)

#print('imgRange', np.min(queryImage), np.max(searchImage))

xMax, yMax, bands = queryImage.shape
print('queryImage', xMax, yMax, bands)

x = 330   # vertical  300(50)  330(80)  50(85)
h = 80
y = 430   # horizontal  200(100)  430(110)  1(50)
w = 110

xStep = int(h/2)
yStep = int(w/2)

nRows = int((xMax-xStep)/xStep)
nCols = int((yMax-yStep)/yStep)

queryPatch = queryImage[x:x+h, y:y+w, :]
print('queryPatch', queryPatch.shape)

# plt.close('all')
# plt.imshow(queryPatch)
# plt.show()

allNodeQuery = treeNodes(queryImage)
print("allNodeQuery", allNodeQuery.shape)
subNodeQuery = patchNodes(allNodeQuery, x, x+h, y, y+w)
print("subNodeQuery", subNodeQuery.shape)

patchHist, patchBinsIdx = nodeHist(subNodeQuery, allNodeQuery)
print("patchHist", patchHist.shape, patchBinsIdx.shape)

allNodeSearch = treeNodes(searchImage)
imgHist, imgBinsIdx = nodeHist(allNodeSearch, allNodeSearch)
print("imgHist", imgHist.shape, imgBinsIdx.shape)

# nBinsPlot = len(patchHist)
# plt.figure()
# plt.subplot(2, 1, 1)
# plt.bar(range(nBinsPlot), imgHist[:nBinsPlot])
# plt.title("Full Histogram")
# plt.subplot(2, 1, 2)
# plt.bar(range(nBinsPlot), patchHist[:nBinsPlot])
# plt.title("Sub Histogram")
# plt.show()

nonZeroBins = np.squeeze(np.nonzero(patchHist))  # index of non zero bins of the patch histogram
print("nonZeroBins", nonZeroBins)
querySubHist = patchHist[nonZeroBins]
querySubHist = querySubHist.reshape((-1,1))
print("querySubHist", querySubHist.shape)
relevantNodeIdx = np.squeeze(np.where(np.isin(imgBinsIdx-1, nonZeroBins)))
nRelevantNode = len(relevantNodeIdx)
print("nRelevantNode", nRelevantNode)

s=0  # This loop adds a row to node_array to show the bin number of each tree node
binIdx = np.zeros((1,nRelevantNode), dtype=np.int8)
for bin in range(len(nonZeroBins)):
    sameBinIdx = np.squeeze(np.where((imgBinsIdx-1)==nonZeroBins[bin]))
    nSameBin = len(sameBinIdx)
    binIdx[0,s:s+nSameBin] = bin
    s += nSameBin

allNodeRelevant = allNodeSearch[:,relevantNodeIdx]
allNodeRelBins = np.concatenate((allNodeRelevant,binIdx))   # last row is bin index
print("allNodeRelBins", allNodeRelBins.shape)

subHistBinN = len(nonZeroBins)
allSubHist = np.zeros((subHistBinN, nRows, nCols))  # sub-histogram for each sliding window
print("allSubHist", allSubHist.shape)

start_time = time.time()
nodeX = np.zeros(nRelevantNode)
for relIdx in range(nRelevantNode):
    currentBin = allNodeRelBins[11,relIdx]

    nodeParent = allNodeRelevant[0,relIdx]
    nodeLevel = allNodeRelevant[2,relIdx]
    nodeParLevel = allNodeSearch[2,nodeParent]
    nodeDifLevel = nodeLevel - nodeParLevel

    nodeArea = allNodeRelBins[3, relIdx]
    nodeVolume = nodeDifLevel * nodeArea
    #print("Node index %d: Volume: %d"%(relIdx, nodeVolume))

    xmin,xmax = allNodeRelBins[6,relIdx], allNodeRelBins[7,relIdx] + 1
    ymin,ymax = allNodeRelBins[9,relIdx], allNodeRelBins[10,relIdx] + 1

    nodeCenterX, nodeCenterY = (xmin+xmax)/2, (ymin+ymax)/2
    nodeWinX, nodeWinY = np.int8(nodeCenterX/xStep), np.int8(nodeCenterY/yStep)
    nodeX[relIdx] = nodeWinX

    if (nodeWinX==0):
        if (nodeWinY==0):
            allSubHist[currentBin, nodeWinX, nodeWinY] += nodeVolume
        if (nodeWinY>0) and (nodeWinY<nCols):
            allSubHist[currentBin, nodeWinX, nodeWinY] += nodeVolume
            allSubHist[currentBin, nodeWinX, nodeWinY-1] += nodeVolume
        if (nodeWinY == nCols):
            allSubHist[currentBin, nodeWinX, nodeWinY-1] += nodeVolume

    if (nodeWinX>0) and (nodeWinX<nRows):
        if (nodeWinY == 0):
            allSubHist[currentBin, nodeWinX, nodeWinY] += nodeVolume
            allSubHist[currentBin, nodeWinX-1, nodeWinY] += nodeVolume
        if (nodeWinY > 0) and (nodeWinY < nCols):
            allSubHist[currentBin, nodeWinX, nodeWinY] += nodeVolume
            allSubHist[currentBin, nodeWinX-1, nodeWinY] += nodeVolume
            allSubHist[currentBin, nodeWinX, nodeWinY-1] += nodeVolume
            allSubHist[currentBin, nodeWinX-1, nodeWinY-1] += nodeVolume
        if (nodeWinY == nCols):
            allSubHist[currentBin, nodeWinX, nodeWinY-1] += nodeVolume
            allSubHist[currentBin, nodeWinX-1, nodeWinY-1] += nodeVolume

    if(nodeWinX==nRows):
        if (nodeWinY == 0):
            allSubHist[currentBin, nodeWinX-1, nodeWinY] += nodeVolume
        if (nodeWinY > 0) and (nodeWinY < nCols):
            allSubHist[currentBin, nodeWinX-1, nodeWinY] += nodeVolume
            allSubHist[currentBin, nodeWinX-1, nodeWinY-1] += nodeVolume
        if (nodeWinY == nCols):
            allSubHist[currentBin, nodeWinX-1, nodeWinY-1] += nodeVolume

#print(np.min(nodeX), np.max(nodeX))

nSearch = nRows*nCols
#allSubHist2D = allSubHist.reshape((subHistBinN, nSearch))
allDist = np.zeros((nSearch,3))
for ix in range(nRows):
    for iy in range(nCols):
        idx = ix * nCols + iy
        allDist[idx, 0] = Distance(querySubHist, allSubHist[:,ix,iy])
        allDist[idx, 1] = ix * xStep
        allDist[idx, 2] = iy * yStep

print("allDist", allDist.shape)
#print(np.unique(allDist[:,2]))

sortDist = sorted(allDist,key=lambda x: x[0])
sortListArr = np.array(sortDist)
print('sortDist', sortListArr.shape)

plt.figure()
plt.plot(sortListArr[:,0],'.')
plt.grid
plt.xlabel("Sorted patches number")
plt.ylabel("chi-squared distances")

nRetrieve = 8
plt.figure()
plt.subplot(3, (nRetrieve+1)/3, 1)
plt.imshow(queryPatch)
plt.axis('off')
plt.title('Query (%d x %d)'%(h,w))
for i in range(nRetrieve):
    dist = sortDist[i][0]
    xr = int(sortDist[i][1])
    yr = int(sortDist[i][2])
    print(xr,yr)
    retPatch = searchImage[xr:xr+h, yr:yr+w, :]
    #print('searchPatch', searchPatch.shape)
    plt.subplot(3, (nRetrieve+1)/3, i+2)
    plt.imshow(retPatch)
    plt.axis('off')
    plt.title("(%d) %.2f"%(i+1, dist))

plt.show()

print("Search time:", time.time()-start_time)