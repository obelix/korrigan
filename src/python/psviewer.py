#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 22:55:41 2020

@author: jerome
"""

# ----------------------------------------

import os
from osgeo import gdal
import numpy as np
import h5py
import time
import sys

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable   # for size of colorbars
import matplotlib.patches as patches # for shape on an image
 
# ======================================================================
# FILE PARAMETERS (put in a separate configuration file)
# ======================================================================

# check for directory of this script itself
scriptdir = os.path.dirname(os.path.realpath(__file__))

# if config file does not exist, then ask user for values and store them in this file
configFile = "psviewer_config.py"
if not os.path.exists(scriptdir+'/'+configFile):
#    print(" *** ERROR *** Configuration file does not exist")
#    exit(0)
    print (" -> First time running this program ? Please provide the following parameters:")
    print ()
    imagedir = input("Please enter directory containing image files (ex: *.tif):\n")
    psdir = input("Please enter directory containing pattern spectra files (*.kor and *.patch):\n")
    rundir = input("Please enter directory containing Korrigan executable files (ex: findPatch):\n")
    resultFile = input("Please enter Korrigan \"result file\" (output of findPatch):\n")
    f = open(scriptdir+'/'+configFile, "w")
    f.write("#configuration file for psviewer.py")
    f.write("imagedir = \"" + imagedir + "\"\n")
    f.write("psdir = \"" + psdir + "\"\n")
    f.write("rundir = \"" + rundir + "\"\n")
    f.write("\n")
    f.write("resultFile = \"" + resultFile + "\"\n")
    f.close()
    print ("File parameters saved in file:", configFile)
    print ("Next time no need to enter them.")
    print ()

from psviewer_config import imagedir, psdir, rundir, resultFile

# ======================================================================
# OTHER PARAMETERS
# ======================================================================

queryBand = 3
searchBand = 1

resIndex = 0

# ----------------------------------------

#rectLinear = False
rectLinear = True
areaLinear = False

debugMode = False

# ======================================================================
# INDEOENDANT FUNCTIONS
# ======================================================================

def SubPSFile2PSFile(subPSFile):
    subPSFileName = subPSFile.rsplit('.',1)[0]    # split on LAST '.'
    subPSFileBegin = subPSFileName.rsplit('_',1)[0]
    PSFile = subPSFileBegin + ".kor"
    return PSFile 

# ----------------------------------------

def PSFile2SubPSFile(PSFile, \
                     subImageLeft, subImageTop,\
                     subImageWidth, subImageHeight):
    PSFileName = PSFile.rsplit('.',1)[0]   # split on LAST '.'
    subPSFile = PSFileName + '_' + str(subImageLeft) + 'x' + str(subImageTop) \
    + '-' + str(subImageWidth) + 'x' + str(subImageHeight) + '.patch'
    return subPSFile

# ----------------------------------------

def subPSFile2ImageFile(subPSFile):
    subPSFileName = subPSFile.rsplit('.',1)[0]    # split on LAST '.'
    subPSFileBegin = subPSFileName.rsplit('_',1)[0]    # split on LAST '_'
    subPSFileNoBand = subPSFileBegin.rsplit('-',1)[0]    # split on LAST '-' (band number)
    imageFile = subPSFileNoBand + ".tif"
    return imageFile

# ----------------------------------------

def PSFile2ImageFile(PSFile):
    PSFileName = PSFile.rsplit('.',1)[0]    # split on LAST '.'
    PSFileNoBand = PSFileName.rsplit('-',1)[0]    # split on LAST '-' (band number)
    imageFile = PSFileNoBand + ".tif"
    return imageFile

# ----------------------------------------

def subPSFile2SubImageCoord (subPSFile):
    # subPSFile = "..._leftxtop-widthxheight.patch"
    subPSFileEnd = subPSFile.split('_')[1]
    subImageCoord = subPSFileEnd.split('.')[0]
    subImageLeftTop = subImageCoord.split('-')[0]
    subImageWidthHeight = subImageCoord.split('-')[1]
    subImageLeft = int(subImageLeftTop.split('x')[0])
    subImageTop = int(subImageLeftTop.split('x')[1])
    subImageWidth = int(subImageWidthHeight.split('x')[0])
    subImageHeight = int(subImageWidthHeight.split('x')[1])
    return subImageLeft, subImageTop, subImageWidth, subImageHeight

# ----------------------------------------

def searchPSFile2Commandline(searchPSFile, \
                                 searchSubImageLeft, searchSubImageTop,\
                                 searchSubImageWidth, searchSubImageHeight):
    commandline = rundir + '/defPatch ' + psdir + '/' + searchPSFile + ' ' + psdir \
    + ' -x ' + str(searchSubImageLeft) + ' -y ' + str(searchSubImageTop) \
    + ' -w ' + str(searchSubImageWidth) + ' -h ' + str(searchSubImageHeight)
    return commandline
    
# ======================================================================
# CLASSES
# ======================================================================

# generic class for any file (result, image, PS)
    
class File:
    
    def __init__(self, dir, file, fileType='', createIfNotExist=False):
        self.dir = dir
        self.file = file
        self.fileType = fileType

        # first check if file exist and stop if not exist
        # ... unless we wish to create it if not exist
        if not createIfNotExist:
            self.checkPath()
        # open and read file
        print ("Loading", self.fileType, "file: ", self.file)

    def path(self):
        return self.dir+'/'+self.file

    def checkPath(self):
        if not os.path.exists(self.dir):
            print ("\n *** ERROR ***", self.fileType, "directory not found: \n", self.dir, "\n")
            exit(0)
        if not os.path.exists(self.dir+'/'+self.file):
            # seconde chance for *.tif extension -> check also *.TIF extension !
            name = os.path.splitext(self.file)[0]
            ext = os.path.splitext(self.file)[1]
            uppercaseFile = name + ext.upper()
            # if uppercase version of file exist, correct filename to uppercase !
            if os.path.exists(self.dir+'/'+uppercaseFile):
#                print (self.file, " -> ", uppercaseFile)
                self.file = uppercaseFile
                return
            # otherwise error
            print ("\n *** ERROR ***", self.fileType, "file not found: \n", 
                   self.dir+'/'+self.file, " (not even ", uppercaseFile, ")\n")
            exit(0)
    
# ----------------------------------------
# class for result file

class Result (File):
    
    def __init__(self, dir, file, fileType='result'):
        super().__init__(dir, file, fileType)

        # open and read file
        f = open(self.path(), "r")
        lines = f.readlines()

        # from header of file: extract sub PS file (*.patch), PS file (*.kor) and nb of results
        querySubPSLine = lines[1]
        self.querySubPSFile = (querySubPSLine.partition('patch: ')[2])[:-1]
        searchPSLine = lines[2]
        self.searchPSFile = (searchPSLine.partition('image candidate: ')[2])[:-1]
        nResLine = lines[6]
        self.nRes = int(nResLine.split(' ')[1])
#        nSearch = int(nResLine.split(' ')[4])

        # from body of file: extract distance, left, top, width and height of every result patch
        self.distance = np.zeros(self.nRes)
        self.left = np.zeros(self.nRes, dtype=int)
        self.top = np.zeros(self.nRes, dtype=int)
        self.width = np.zeros(self.nRes, dtype=int)
        self.height = np.zeros(self.nRes, dtype=int)

        headerNLine = 7
        l = 0
        # line string format: "distance left, top	widthxheight"
        for line in lines[headerNLine:]:
            self.distance[l] = float(line.split()[0])
            leftTop = line.split()[1]
            self.left[l] = int(leftTop.split(',')[0])
            self.top[l] = int(leftTop.split(',')[1])
            widthHeight = line.split()[2]
            self.width[l] = int(widthHeight.split('x')[0])
            self.height[l] = int(widthHeight.split('x')[1])
            l += 1
        f.close()
        
    def searchSubImage(self, index):
        return result.left [index], result.top [index], result.width [index], result.height [index]
        
# ----------------------------------------

class Image (File):

    def __init__(self, dir, file, fileType='image'):
        super().__init__(dir, file, fileType)

        # open and read image
        gdal.AllRegister()
        driver = gdal.GetDriverByName('SRTMHGT')
        driver.Register()
        self.dataset = gdal.Open(self.path(), gdal.GA_ReadOnly)
        self.nCol = self.dataset.RasterXSize
        self.nRow = self.dataset.RasterYSize
        self.nBand = self.dataset.RasterCount

    def data(self, band):
        imageBand = self.dataset.GetRasterBand(band)
        return imageBand.ReadAsArray()

    def computeOptMinMax(self, band, nSigma=2):
        imageBand = self.dataset.GetRasterBand(band)
        imageStat = imageBand.GetStatistics(True, True)
        imageMin = imageStat[0]
        imageMax = imageStat[1]
        imageMean = imageStat[2]
        imagesStddev = imageStat[3]
        imageOptMin = max (imageMean - nSigma*imagesStddev, imageMin)
        imageOptMax = min (imageMean + nSigma*imagesStddev, imageMax)
        return imageOptMin, imageOptMax

# ----------------------------------------

class ImageView:
    
    def __init__(self, image, axes, title='image', 
                 left=-1, top=-1, width=0, height=0,
                 imageOptMin=-1, imageOptMax=-1,
                 showQuery=False, showSearch=False, showResults=False, showCoord=False, showIndex=False):
        self.image = image
        self.axes = axes
        self.title = title
        self.left = left
        self.top = top
        self.width = width
        self.height = height
        self.imageOptMin = imageOptMin
        self.imageOptMax = imageOptMax
        self.showQuery = showQuery
        self.showSearch = showSearch
        self.showResults = showResults
        self.showCoord = showCoord
        self.showIndex = showIndex

    def update(self, left, top, width, height):
        self.left = left
        self.top = top
        self.width = width
        self.height = height

    def show(self, band=1):

        # if optimal intensity range not given, compute it
        if (self.imageOptMin == -1) or (self.imageOptMax == -1):
            self.imageOptMin, self.imageOptMax = self.image.computeOptMinMax(band)

        if (self.left == -1) or (self.top == -1):
            left = 0
            right = self.image.nCol - 1
            top = 0
            bottom = self.image.nRow - 1
        else:
            left = self.left
            right = self.left+self.width-1
            top = self.top
            bottom = self.top+self.height-1
        subimage = self.image.data(band)[top:bottom, left:right]
        self.axes.clear()
        self.axes.imshow(subimage,
                     extent=[left, right, bottom, top],
                     cmap='gray', vmin=self.imageOptMin, vmax=self.imageOptMax)
        title = self.title
        if self.showCoord:
            title += ': origin=(' + str(self.left) + ';' + str(self.top) + ')' + \
            ' size=(' + str(self.width) + 'x' + str(self.height) + ')'
        if self.showIndex:
            title += '\n(result '+str(resIndex+1)+'/'+str(nRes)+')'
        self.axes.set_title(title)

        # show query sub-image (draw a color rectangle)
        if self.showQuery:
            self.drawRectangle(left=querySubImageLeft, top=querySubImageTop, 
                                          width=querySubImageWidth, height=querySubImageHeight, 
                                          color='blue', alpha=0.4)

        # show all results sub-images (draw color rectangles)
        if self.showResults:
            for r in range(result.nRes):
                searchImageView.drawRectangle(left=result.left[r], top=result.top[r], 
                                              width=result.width[r], height=result.height[r], 
                                              color='yellow')

        # show search sub-image (draw a color rectangle)
        if self.showSearch:
            searchImageView.drawRectangle(left=result.left[resIndex], top=result.top[resIndex], 
                                          width=result.width[resIndex], height=result.height[resIndex], 
                                          color='green', alpha=0.6)
            
            
    def drawRectangle(self, left, top, width, height, color='r', alpha=0.2):
        # Create a Rectangle corresponding to the sub image
        subimagerect = patches.Rectangle((left, top), width, height, 
                                         linewidth=1, edgecolor='None', facecolor=color, alpha=alpha)
        # Add the rectangle
        self.axes.add_patch(subimagerect)

        subimagerect = patches.Rectangle((left, top), width, height, 
                                         linewidth=1, edgecolor="yellow", facecolor='None')
        # Add the rectangle
        self.axes.add_patch(subimagerect)

# ----------------------------------------

class PS(File):

    def __init__(self, dir, file, fileType='PS', createIfNotExist=False):
        super().__init__(dir, file, fileType, createIfNotExist)
        
        # if file does not exist then create if
        if not os.path.exists(self.dir+'/'+self.file):
            self.createFile()

        # members
        MAX_PIXEL_BIN = 100000000
        # on x-axis
        if rectLinear:
            self.binRectCount = 30
            self.binRectMin = 0.
            self.binRectMax = 1.
        else:
            self.binRectCount = 30
            self.binRectMin = 1./MAX_PIXEL_BIN
            self.binRectMax = 1.
        
        # on y-axis
        self.binAreaCount = 30
        self.binAreaMin = 1
        self.binAreaMax = MAX_PIXEL_BIN

        # check file extension (*.kor -> full PS or *.patch -> sub PS)
        ext = os.path.splitext(os.path.realpath(self.file))[1]
        if ext == '.kor':
            # open and read PS
            f = h5py.File(self.path(), 'r')
            PS1D = np.array(f['PS'][:])
        if ext == '.patch':
            # if file does not exist then create it
            if not os.path.exists(self.path()):
                self.createFile()
            # open and read PS
            f = h5py.File(self.path(), 'r')
            psBinIdx = np.array(f['patchPSBinIdx'][:])
            psBinVal = np.array(f['patchPSBinVal'][:])
            nNode = psBinIdx.shape[0]
            # deduce PS array
            PS1D = np.zeros(self.binRectCount * self.binAreaCount)
            for k in range(nNode):
                	PS1D [ psBinIdx[k] ] += psBinVal[k]
        # PS 1D -> PS 2D
        self.PSTrans = PS1D.reshape(self.binRectCount, self.binAreaCount)
        # C++ is column-major, but numpy is row-major, so transpose
        # PS = np.transpose(PSTrans)
        
    def createFile(self)        :
        start_time = time.time()
        
        print ("Creating", self.fileType, "file: ", self.file)
        
        subImageLeft, subImageTop, subImageWidth, subImageHeight = \
            subPSFile2SubImageCoord(self.file)
        
        # convert sub-PS filename -> PS filename
        searchPSFile = SubPSFile2PSFile(self.file)
        commandline = searchPSFile2Commandline(searchPSFile, 
                                               subImageLeft, subImageTop,
                                               subImageWidth, subImageHeight)
#        print (commandline)
        os.system(commandline)
        
        print("   (%0.1f seconds)" % (time.time() - start_time))
        
        
    def data(self):
        return self.PSTrans

    def computeMinMax(self):
        psMin = np.min (self.PSTrans)
        psMax = np.max (self.PSTrans)
        return psMin, psMax

    def computeOptMinMax(self, nSigma=3):
        psMin = np.min (self.PSTrans)
        psMax = np.max (self.PSTrans)
        psMean = np.mean(self.PSTrans)
        psStddev = np.std(self.PSTrans)
        psOptMin = max (psMean - nSigma*psStddev, psMin)
        psOptMax = min (psMean + nSigma*psStddev, psMax)
        return psOptMin, psOptMax

# ----------------------------------------

class PSView:
    
    def __init__(self, ps, axes, title='PS', 
                 psMin=-1, psMax=-1, showDistance=False, showIndex=False):
        self.ps = ps
        self.axes = axes
        self.title = title
        self.psMin = psMin
        self.psMax = psMax
        self.showDistance = showDistance
        self.showIndex = showIndex
        self.cmap = cm.YlOrRd
        self.cmap.set_bad(color='gray')

    def update(self, ps=None):
        self.ps = ps

    def show(self):
        
        # if optimal intensity range not given, compute it
        if (self.psMin == -1) or (self.psMax == -1):
            self.psMin, self.psMax = self.ps.computeMinMax()

        PSTransMask = np.ma.masked_where(self.ps.data() == 0, self.ps.data())
        im = self.axes.imshow(PSTransMask, 
#                              extent=[self.ps.binRectMin, self.ps.binRectMax, self.ps.binAreaMin, self.ps.binAreaMax],
                              origin='lower', aspect='auto', cmap=self.cmap, vmin=self.psMin, vmax=self.psMax)

        title = self.title
        if self.showDistance:
            distance = result.distance[resIndex]
            title += ': distance = '+str(distance)
        if self.showIndex:
            title += '\n(result '+str(resIndex+1)+'/'+str(nRes)+')'
        self.axes.set_title(title)

        # X-axis
        self.axes.set_xlabel('Rectangularity')
#        if not rectLinear:
#            self.axes.set_xscale('log')
        if rectLinear:
            	tickLabelsRect = ['0.', '0.2', '0.4', '0.6', '0.8', '1.0']
            	tickValuesRect = list(np.float_(tickLabelsRect))
            	ticksPosRect = (tickValuesRect-min(tickValuesRect)) / (max(tickValuesRect)-min(tickValuesRect))  * (self.ps.binRectCount-1)
        else:
            	tickLabelsRect = ['1e-8', '1e-6', '1e-4', '1e-2', '1e0']
            	tickValuesRect = list(np.float_(tickLabelsRect))
            	tickLogvaluesRect = np.log10(tickValuesRect)
            	ticksPosRect = (tickLogvaluesRect-min(tickLogvaluesRect)) / (max(tickLogvaluesRect)-min(tickLogvaluesRect))  * (self.ps.binRectCount-1)
        ### ax.ticklabel_format(style='sci',axis='y')
        self.axes.set_xticklabels(tickLabelsRect)
        self.axes.set_xticks(ticksPosRect)

        # Y-axis
        self.axes.set_ylabel('Area')
#        if not areaLinear:
#            self.axes.set_yscale('log')
        tickLabelsArea = ['1e0', '1e2', '1e4', '1e6', '1e8']
        tickValuesArea = list(np.float_(tickLabelsArea))
        tickLogvaluesArea = np.log10(tickValuesArea)
        ticksPosArea = (tickLogvaluesArea-min(tickLogvaluesArea)) / (max(tickLogvaluesArea)-min(tickLogvaluesArea))  * (self.ps.binAreaCount-1)
        ### ax.ticklabel_format(style='sci',axis='x')
        self.axes.set_yticklabels(tickLabelsArea)
        self.axes.set_yticks(ticksPosArea)

        # colorbar
        divider = make_axes_locatable(self.axes)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        fig.colorbar(im, cax=cax)
        
# ======================================================================
# READ RESULT FILE
# ======================================================================

print ("PATTERN SPECTRA VIEWER")
print ()

result = Result(psdir, resultFile)

querySubPSFile = result.querySubPSFile
queryPSFile = SubPSFile2PSFile (querySubPSFile)
queryImageFile = subPSFile2ImageFile (querySubPSFile)

searchPSFile = result.searchPSFile
#searchSubPSFile = PSFile2SubPSFile(searchPSFile)
searchImageFile = PSFile2ImageFile (searchPSFile)
print()

# ----------------------------------------

nRes = result.nRes

querySubImageLeft, querySubImageTop, querySubImageWidth, querySubImageHeight = \
subPSFile2SubImageCoord(querySubPSFile)

searchSubImageLeft, searchSubImageTop, searchSubImageWidth, searchSubImageHeight = \
result.searchSubImage(resIndex)

# ----------------------------------------

if debugMode:
    print ()
    print (" -> queryImageFile: ", queryImageFile)
    print (" -> queryPSFile: ", queryPSFile)
    print ("|   querySubPSFile: ", querySubPSFile)
    print ()
    print (" -> searchImageFile: ", searchImageFile)
    print ("|   searchPSFile: ", searchPSFile)
    #print ("searchSubPSFile: ", searchSubPSFile)
    print ()
    print ("resIndex / nRes: ", resIndex, "/", nRes)
    print ("querySubImage: origin =", querySubImageLeft, querySubImageTop, ", size =", querySubImageWidth, querySubImageHeight)
    print ("searchSubImage: origin =", searchSubImageLeft, searchSubImageTop, ", size =", searchSubImageWidth, searchSubImageHeight)

# ======================================================================
# READ DATA (IMAGES AND PS)
# ======================================================================

queryImage = Image(imagedir, queryImageFile)
queryPS = PS(psdir, queryPSFile)
querySubPS = PS(psdir,querySubPSFile)

searchImage = Image(imagedir, searchImageFile)
searchPS = PS(psdir, searchPSFile)

# ----------------------------------------
# creating sub-ps filename (*.patch) for search image

searchSubPSFile = PSFile2SubPSFile(searchPSFile, 
                                   searchSubImageLeft, searchSubImageTop,\
                                   searchSubImageWidth, searchSubImageHeight)

   
# ----------------------------------------

# rad sub-ps file. If not exist create it !
searchSubPS = PS(psdir,searchSubPSFile, createIfNotExist=True)

# ----------------------------------------
# automatically adjusting intensity range for display

# for image intensity ranges: query subimage range = query image range
queryImageOptMin, queryImageOptMax = queryImage.computeOptMinMax(queryBand)
searchImageOptMin, searchImageOptMax = searchImage.computeOptMinMax(searchBand)

# for PS intensity ranges: common scale for all
#queryPSMin, queryPSMax = queryPS.computeMinMax()
#querySubPSMin, querySubPSMax = querySubPS.computeMinMax()
#searchPSMin, searchPSMax = searchPS.computeMinMax()
#searchSubPSMin, searchSubPSMax = searchSubPS.computeMinMax()

queryPSMin, queryPSMax = queryPS.computeMinMax()
querySubPSMin, querySubPSMax = querySubPS.computeMinMax()
searchPSMin, searchPSMax = searchPS.computeMinMax()
searchSubPSMin, searchSubPSMax = searchSubPS.computeMinMax()

commonPSMin = min ([queryPSMin, searchPSMin])
commonPSMax = max ([queryPSMax, searchPSMax])
commonSubPSMin = min ([querySubPSMin, searchSubPSMin])
commonSubPSMax = max ([querySubPSMax, searchSubPSMax])

# ======================================================================
# GRAPHICAL DISPLAY
# ======================================================================

# declaration of fake variables to allow first call of fucntion "change_view"
#searchSubPS = None
#queryImageView = None
#queryPSView = None
#querySubImageView = None
#querySubPSView = None
#searchImageView = None
#searchPSView = None
#searchSubImageView = None
#searchSubPSView = None

# ----------------------------------------

def change_view (event):
    # access global variables outside this function
    global resIndex, searchSubImageLeft, searchSubImageTop, searchSubImageWidth, searchSubImageHeight, searchSubImageView, \
        searchBand, searchSubPSFile, searchSubPS
#    print('press', event.key)
    sys.stdout.flush()
    resIndexChanged = False
    # if KEY 'n' -> next result
    if event.key == 'n':
        # update result index and sub-image coordinates
        resIndex += 1
        resIndex = resIndex % nRes
        resIndexChanged = True
    # if KEY 'p' -> previous result
    if event.key == 'p':
        # update result index and sub-image coordinates
        resIndex -= 1
        resIndex = resIndex % nRes
        resIndexChanged = True
    if resIndexChanged:
        # update sub-image coordinates
#        print("resIndex = ", resIndex)
        searchSubImageLeft, searchSubImageTop, searchSubImageWidth, searchSubImageHeight = \
        result.searchSubImage(resIndex)
        # 1) update search image view (because sub-image rectangles have changed)
        searchImageView.show(searchBand)
        # 2) update search sub-image view
        searchSubImageView.update(searchSubImageLeft, searchSubImageTop, searchSubImageWidth, searchSubImageHeight)
        searchSubImageView.show(searchBand)
        # update search sub-PS filename
        searchSubPSFile = PSFile2SubPSFile(searchPSFile, 
                                   searchSubImageLeft, searchSubImageTop,\
                                   searchSubImageWidth, searchSubImageHeight)
        # 3a) update search sub-PS data
        # ... if file does not exist, then create it  !
        searchSubPS = PS(psdir, searchSubPSFile, createIfNotExist=True)
        # 3b) update search sub-PS view
#        print (sys.getsizeof(searchSubPS))
        searchSubPSView.update(searchSubPS)
        searchSubPSView.show()
        # validate all graphical chnages
        fig.canvas.draw()

# ----------------------------------------

print ()
print ("Creating display...")

#visuList=['image', 'ps', 'subimage', 'subps']
visuList=['image', 'subimage', 'subps']
nVisu = len(visuList)
imageList=['query', 'search']
imageFileList=[queryImageFile, searchImageFile]
nImage = len(imageList)

#fig, big_axes = plt.subplots(nrows=nImage, ncols=1, sharey=True) 
#
#for row, big_ax in enumerate(big_axes, start=1):
#    big_ax.set_title("Subplot row %s \n" % row, fontsize=16)

fig, axesList = plt.subplots(nrows=nImage, ncols=nVisu)
fig.canvas.set_window_title('PS Viewer') 

margin = 0.05
wspace = 0.3
hspace = 0.3
plt.subplots_adjust(left=margin, right=1.-margin, top=1.-margin, bottom=margin, 
                    wspace=wspace, hspace=hspace)

for i in range(nImage):
    plt.figtext(0.5, 0.99 - i/nImage, imageList[i] + ' image : ' + imageFileList[i], ha='center', va='center', color='blue', fontsize=12)

# ----------------------------------------

fig.canvas.mpl_connect('key_press_event', change_view)
    
# ----------------------------------------
## draw all the figures in the page
#def pageShow():
#    print ("pageShow: nImage=", nImage, " nVisu=", nVisu)

i=0
for currentImage in imageList:
    v=0
    for currentVisu in visuList:
        if nVisu > 1:
            if nImage > 1:
                axes = axesList[i,v]
            else:
                axes = axesList[v]
        else:
            axes = axesList
            
        if currentVisu == 'image':
            if currentImage == 'query':
                queryImageView = ImageView(queryImage, axes, title='Full image', 
                                           imageOptMin=queryImageOptMin, imageOptMax=queryImageOptMax, 
                                           showQuery=True)
                queryImageView.show(queryBand)
            if currentImage == 'search':
                searchImageView = ImageView(searchImage, axes, title='Full image',
                                           imageOptMin=searchImageOptMin, imageOptMax=searchImageOptMax,
                                           showSearch=True, showResults=True)
                searchImageView.show(searchBand)
            
        if currentVisu == 'ps':
            if currentImage == 'query':
                queryPSView = PSView(queryPS, axes, title='Full PS',
                                     psMin=commonPSMin, psMax=commonPSMax)
                queryPSView.show()
            if currentImage == 'search':
                searchPSView = PSView(searchPS, axes, title='Full PS',
                                      psMin=commonPSMin, psMax=commonPSMax)
                searchPSView.show()
            
        if currentVisu == 'subimage':
            if currentImage == 'query':
                querySubImageView = ImageView(queryImage, axes, title='Sub image', 
                                              imageOptMin=queryImageOptMin, imageOptMax=queryImageOptMax, 
                                              left=querySubImageLeft, top=querySubImageTop, 
                                              width=querySubImageWidth, height=querySubImageHeight,
                                              showCoord=True)
                querySubImageView.show(queryBand)
            if currentImage == 'search':
                searchSubImageView = ImageView(searchImage, axes, title='Sub image', 
                                               imageOptMin=searchImageOptMin, imageOptMax=searchImageOptMax,
                                               left=searchSubImageLeft, top=searchSubImageTop, 
                                               width=searchSubImageWidth, height=searchSubImageHeight, 
                                               showCoord=True, showIndex=True)
                searchSubImageView.show(searchBand)
            
        if currentVisu == 'subps':
            if currentImage == 'query':
                querySubPSView = PSView(querySubPS, axes, title='Sub PS',
                                        psMin=commonSubPSMin, psMax=commonSubPSMax)

                querySubPSView.show()
            if currentImage == 'search':
                searchSubPSView = PSView(searchSubPS, axes, title='Sub PS',
                                        psMin=commonSubPSMin, psMax=commonSubPSMax,
                                        showDistance=True, showIndex=True)

                searchSubPSView.show()
            
        v += 1
    i += 1

# ----------------------------------------

#pageShow()

# ----------------------------------------

# start with maximized window
mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
# mng.window.state('zoomed')
# mng.frame.Maximize(True)
### mng.window.showMaximized()
# mng.full_screen_toggle() # OK: full screen
#print ("***: ", plt.get_backend())

plt.show()

# ----------------------------------------
