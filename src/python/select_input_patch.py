
# python3 test_image_gdal.py

import matplotlib.pyplot as plt
import numpy as np
from osgeo import gdal
from matplotlib.widgets  import RectangleSelector
import os
import subprocess

filename="/home/more/Images/arles.tif"
image = gdal.Open(filename)

row = image.RasterYSize
col = image.RasterXSize
band = image.RasterCount

imarray = np.zeros([row, col, band], dtype=np.uint16)
for b in range(band):
        imarray[:, :, b] = np.array(image.GetRasterBand(b+1).ReadAsArray())
        
print ("image: ", filename)
print (" nrow: ", row, " ncol: ", col, " nband: ", band)

plt.figure()
plt.imshow(imarray[:,:,0], cmap='gray')

print("Please click twice to select a rectangular zone:")
p1,p2 = plt.ginput(2)
x1 = int(round(p1[0]))
y1 = int(round(p1[1]))
x2 = int(round(p2[0]))
y2 = int(round(p2[1]))

#print("start point (", x1, ";", y1, ")")
#print("end point (", x2, ";", y2, ")")

# convert (x1,y1) and (x2,y2) to x-left, y-top, x-width and y-height
xl = min(x1,x2)
yt = min(y1,y2)
xw = abs(x2-x1)
yh = abs(y2-y1)

print("start point (", xl, ";", yt, ")")
print("size (", xw, ";", yh, ")")

# save them into file
configfile="../bash/test_cnesPS_2.cfg"
f = open(configfile, "w")
f.write("# query patch coordinates\n")
f.write("x1="+str(xl)+"\n")
f.write("y1="+str(y1)+"\n")
f.write("xw="+str(xw)+"\n")
f.write("yh="+str(yh)+"\n")
f.close()

# finally calls the main bash scripts
# *** NOT WORKING ***
# bashdir="../bash"
# bashfile="test_cnesPS"
# os.chdir(bashdir)
# subprocess.call("pwd", shell=True)
# subprocess.call("/home/more/git/korrigan/src/bash/"+bashfile, shell=True)

exit()

