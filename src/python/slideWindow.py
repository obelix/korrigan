import siamxt
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from scipy import stats
from operator import itemgetter
from scipy.stats import chisquare
import time

def PatternSpectra(imgGray):
    Bc = np.ones((3,3), dtype=bool)  # connectivity
    mxt = siamxt.MaxTreeAlpha(imgGray,Bc)   # Max tree

    nodeParent = mxt.node_array[0,:]
    nodeLevel = mxt.node_array[2,:]
    nodeParLevel = mxt.node_array[2,nodeParent]
    nodeDifLevel = nodeLevel - nodeParLevel

    nodeEcce = mxt.computeEccentricity()[2]
    nodeEcce[np.isnan(nodeEcce)] = 0
    nodeEccVol = nodeDifLevel * nodeEcce
    #print(nodeEccVol.shape)

    nodeArea = mxt.node_array[3,:]
    nodeVolume = nodeDifLevel * nodeArea

    nodeRect = mxt.computeRR()
    nodeRectVol = nodeDifLevel * nodeRect

    nodeGrayAvg = mxt.computeNodeGrayAvg()
    nodeGrayVar = mxt.computeNodeGrayVar()
    nodeHeight = mxt.computeHeight()
    nodeStable = mxt.computeStabilityMeasure()

    nodeAttr = [nodeArea, nodeEcce, nodeRect, nodeLevel, nodeGrayAvg, nodeGrayVar, nodeHeight, nodeStable]

    nBin = 30
    attr1 = 0
    begin = np.min(nodeAttr[attr1])
    end = np.max(nodeAttr[attr1])
    log_edges1 = np.logspace(np.log10(begin+1e-10), np.log10(end), num=nBin)
    attr2 = 2
    begin = np.min(nodeAttr[attr2])
    end = np.max(nodeAttr[attr2])
    log_edges2 = np.logspace(np.log10(begin+1e-10), np.log10(end), num=nBin)
    attr3 = 7
    begin = np.min(nodeAttr[attr3])
    end = np.max(nodeAttr[attr3])
    log_edges3 = np.logspace(np.log10(begin+1e-10), np.log10(end), num=nBin)

    hist1d, edges, binIdxs = stats.binned_statistic(nodeAttr[attr1], nodeVolume, statistic='sum', bins=nBin)
    ret2 = stats.binned_statistic_2d(nodeAttr[attr1], nodeAttr[attr2], nodeVolume,
                                    statistic='sum', bins=[log_edges1,log_edges2])
    hist2d = ret2.statistic.flatten()
    ret3 = stats.binned_statistic_dd([nodeAttr[attr1], nodeAttr[attr2], nodeAttr[attr3]], nodeVolume,
                                     statistic='sum', bins=[log_edges1, log_edges2, log_edges3])
    hist3d = ret3.statistic.flatten()

    return hist2d

def descriptor(imgRGB):
    r, g, b = imgRGB[:,:,0], imgRGB[:,:,1], imgRGB[:,:,2]
    imgGray = np.uint16(0.2989 * r + 0.5870 * g + 0.1140 * b)
    negImg = np.max(imgGray) - imgGray
    medImg = np.uint16(np.abs(imgGray-np.median(imgGray)))

    #print('Gray:', imgGray.shape)
    #plt.imshow(negImg, cmap='gray')

    psMax = PatternSpectra(imgGray)
    psMin = PatternSpectra(negImg)
    psMed = PatternSpectra(medImg)

    ps = np.concatenate((psMax,psMin,psMed))
    return psMed

def Distance(x,y):
    statistic = 0.5*np.sum((x-y)**2/(x+y+1e-6))
    #statistic, p = chisquare(x,y+1e-6)
    #statistic, p= stats.ks_2samp(x,y)
    #dist = stats.wasserstein_distance(x,y)
    return statistic

path = ""

img_png_q = 'image01.png'
img_png_s = 'image11.png'

imgQuery = io.imread(path + img_png_q)
imgSearch = io.imread(path + img_png_s)

print('imgRange', np.min(imgSearch), np.max(imgSearch))

xMax, yMax, bands = imgSearch.shape
print('imgSearch', xMax, yMax, bands)

#ds = gdal.Open(path + img_tif)
#band = ds.GetRasterBand(4)
#arr = band.ReadAsArray()

#print(arr.shape)
#print(arr.min(), arr.max())
#a=np.sort(np.unique(arr.flatten()))
#print(a[-10:])

x = 50   # vertical  300(50)  330(80)  50(85)
h = 85
y = 1   # horizontal  200(100)  430(110)  1(50)
w = 50

queryPatch = imgQuery[x:x+h, y:y+w, :]
print('queryPatch', queryPatch.shape)

# plt.imshow(queryPatch)
# plt.show()

xStep = int(h/2)
yStep = int(w/2)

start_time = time.time()

nRows = int((xMax-h)/xStep)+1
nCols = int((yMax-w)/yStep)+1
psDist = np.zeros((nRows*nCols, 3))
print('psDist', psDist.shape)
psQuery = descriptor(queryPatch)
idx = 0
for xs in range(1, xMax-h, xStep):
    for ys in range(1, yMax-w, yStep):
        #print(xs, ys)
        searchPatch = imgSearch[xs:xs+h, ys:ys+w,:]
        psSearch = descriptor(searchPatch)
        psDist[idx, 0] = Distance(psQuery, psSearch)
        psDist[idx, 1] = xs
        psDist[idx, 2] = ys
        print('%7.2f, %5d, %5d' % (psDist[idx, 0], psDist[idx, 1], psDist[idx, 2]))
        idx += 1

sortDist = sorted(psDist,key=lambda x: x[0])
#sortDist = sorted(psDist,key=itemgetter(0))
#print(np.array(sortDist))

nRetrieve = 8
plt.subplot(3, (nRetrieve+1)/3, 1)
plt.imshow(queryPatch)
plt.axis('off')
plt.title('Query (%d x %d)'%(h,w))
for i in range(nRetrieve):
    dist = sortDist[i][0]
    xr = int(sortDist[i][1])
    yr = int(sortDist[i][2])
    print(xr,yr)
    retPatch = imgSearch[xr:xr+h, yr:yr+w, :]
    #print('searchPatch', searchPatch.shape)
    plt.subplot(3, (nRetrieve+1)/3, i+2)
    plt.imshow(retPatch)
    plt.axis('off')
    plt.title("(%d) %.2f"%(i+1, dist))

plt.show()

print("Search time:", time.time()-start_time)
