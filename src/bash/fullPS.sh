#!/bin/bash

# TODO add all korrigan option

usage () {
    echo `basename "$0"` " [-h] [-help] {srcImgDir|srcImg.tif} dstPSDir"
    echo "    generate all pattern spectra"
    echo "    -h"
    echo "    -help Display this help."
}

if test "$#" -le 1
then
    usage
    exit
fi

case "$1" in
    '-h' | '-help' | '--help' )
	usage
	shift
	exit;;
esac

if test "$#" -ne 2
then
    usage
    exit
fi

inputName=$1
outputName=$2

if test -d ${inputName}
then
    for file in "${inputName}/"*
    do
	echo createPS "${file}" "${outputName}"
    done
    exit
fi

if test -r ${inputName}
then
    echo createPS "${inputName}" "${outputName}"
    exit
fi

usage
