////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * PatternSpectra.cpp
 *
 *  Created on: 14 nov. 2019
 *      Author: raimbaul
 */

#include "PatternSpectra.hpp"
#ifndef H5_NO_NAMESPACE
using namespace H5;
#endif

#include <iostream>

using namespace obelix;
using namespace obelix::triskele;

PatternSpectra::PatternSpectra(
			       int dim1, int dim2,
			       double range1, double range2,
			       int xpos, int ypos,
			       int xsize, int ysize,
			       std::string name){

  dim_1= dim1;
  dim_2= dim2;
  bins= std::vector<uint32_t>(dim1*dim2,0);
  range_1= range1;
  range_2= range2;
  x_pos= xpos;
  y_pos= ypos;
  x_size= xsize;
  y_size= ysize;
  image_filename= name;
}

int
PatternSpectra::fill_from_matrix(
				 uint32_t matrix[]){

  for(int i1=0; i1<dim_1; i1++){
    for(int i2=0; i2<dim_2; i2++){
      assign_count(matrix[i1*dim_2+i2],i1,i2) ;
    }
  }
  return 0;
}

double
PatternSpectra::compareTo(PatternSpectra & ps){

  if ((dim_1 != ps.dim_1) || (dim_2 != ps.dim_2)) return -1; // must have the same dimensions
  double distance= 0;
  for(int i1=0; i1<dim_1; i1++){
    for(int i2=0; i2<dim_2; i2++){
      double X= get_count(i1,i2);
      double Y= ps.get_count(i1,i2);
      distance+= (X-Y)*(X-Y) / (X+Y);
    }
  }
  return distance;
}

int
PatternSpectra::open_read_h5(const std::string filename){
  if (! H5File::isHdf5(filename)) return 2;
  ps_h5filename= H5std_string( filename );
  try{
    // Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
    Exception::dontPrint();
    ps_h5file= H5File(ps_h5filename, H5F_ACC_RDONLY );
  } catch (FileIException & error){
    error.printErrorStack();
    return 1;
  }
  return 0;
}

int
PatternSpectra::open_write_h5(const std::string filename){
  ps_h5filename= H5std_string( filename );
  try{
    // Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
    Exception::dontPrint();
    ps_h5file= H5File(ps_h5filename, H5F_ACC_TRUNC );
  } catch (FileIException & error){
    error.printErrorStack();
    return 1;
  }
  return 0;
}

int
PatternSpectra::close_h5(){
  try{
    // Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
    ps_h5file.close();
  } catch (FileIException & error){
    error.printErrorStack();
    return 1;
  }
  return 0;
}

const H5std_string h5_dataset_name( "PS" );
const int h5_rank= 1;

int
PatternSpectra::load_from_h5(){

  try
    {
      // Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
      Exception::dontPrint();
      char group_X[64], group_Y[64];
      sprintf(group_X,"X=%d",x_pos);
      sprintf(group_Y,"Y=%d",y_pos);
      Group h5_groupX (ps_h5file.openGroup(group_X));
      Group h5_groupY (h5_groupX.openGroup(group_Y));
      DataSet h5_dataset = h5_groupY.openDataSet( h5_dataset_name);
      DataType h5_datatype= h5_dataset.getDataType();
      if (! (h5_datatype == PredType::NATIVE_UINT32)) return 1;
      DataSpace h5_dataspace= h5_dataset.getSpace();
      if (h5_dataspace.getSimpleExtentNdims() != h5_rank) return 2;
      hsize_t h5_dataset_dimensions[h5_rank];
      h5_dataspace.getSimpleExtentDims(h5_dataset_dimensions);
      DataSpace mem_dataspace( h5_rank, h5_dataset_dimensions );
      bins= std::vector<uint32_t>(h5_dataset_dimensions[0]);
      h5_dataset.read(bins.data(), PredType::NATIVE_UINT32, mem_dataspace, h5_dataspace);
      h5_dataset.close();
      h5_groupY.close();
      h5_groupX.close();
      h5_dataspace.close();
    }catch( FileIException & error ){ // catch failure caused by the H5File operations
    error.printErrorStack();
    return 2;
  }catch( DataSetIException & error ){ // catch failure caused by the DataSet operations
    error.printErrorStack();
    return 3;
  }catch( DataSpaceIException & error ){ // catch failure caused by the DataSpace operations
    error.printErrorStack();
    return 4;
  }catch( ReferenceException & error ){ // catch failaure caused by Group operations
    error.printErrorStack();
    return 4;
  }
  return 0;
}

int
PatternSpectra::store_to_h5(){

  const hsize_t h5_dataset_dimensions[h5_rank]= { (hsize_t) dim_1 * dim_2 };
  const DataType h5_datatype= DataType(PredType::NATIVE_UINT32);
  Exception::dontPrint();// Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
  DataSpace h5_dataspace( h5_rank, h5_dataset_dimensions );
  char group_X[100], group_Y[100];
  sprintf(group_X,"X=%d",x_pos);
  Group h5_groupX;
  try {
    h5_groupX= ps_h5file.openGroup(group_X);
  } catch (FileIException & e) { // group dont exist
    //std::cout << "create group " << group_X << std::endl;
    h5_groupX= ps_h5file.createGroup(group_X);
  }
  sprintf(group_Y,"Y=%d",y_pos);
  Group h5_groupY;
  try {
    h5_groupY= h5_groupX.openGroup(group_Y);
  } catch (GroupIException & e) { // dont exist
    //std::cout << "create group " << group_Y << std::endl;
    h5_groupY= h5_groupX.createGroup(group_Y);
  }
  try{
    DataSet h5_dataset = h5_groupY.createDataSet( h5_dataset_name,h5_datatype,h5_dataspace );
    h5_dataset.write(bins.data(),h5_datatype);
    h5_dataset.close();
    h5_groupY.close();
    h5_groupX.close();
    h5_dataspace.close();
  }
  catch( DataSetIException & error )// catch failure caused by the DataSet operations
    {
      error.printErrorStack();
      return 3;
    }
  return 0;
}


