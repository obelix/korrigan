////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

// ./fullPS --seqTileFlag --timeFlag --countFlag ../arles.tif ../result/ -w 1000 -h 1000

#include <iostream>
#include <boost/filesystem.hpp>
#include <algorithm>

#include "OptGPS.hpp"

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"
#include "Attributes/RectangularityAttributes.hpp"

#include "Korrigan.hpp"
#include "KorriganStats.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace obelix;
using namespace obelix::korrigan;

// ========================================
template<typename InPixelT>
void
readRaster (Raster<InPixelT, 2> &raster, const IImage<2> &inputImage, const OptGPS &optGPS) {
  inputImage.readBand (raster, optGPS.selectedBand, optGPS.optCrop.topLeft, optGPS.optCrop.size);
}

// ========================================
template<typename InPixelT>
void
precompute (IImage<2> &inputImage, const string &outputFileName, const OptGPS &optGPS) {
  DEF_LOG ("precompute", "");
  TreeType treeType = MED;

  Raster<InPixelT, 2> raster;
  readRaster (raster, inputImage, optGPS);
  const DimImg pixelCount (raster.getSize ().getPixelsCount ());
  DimCore threadPerImage (optGPS.threadPerImage), tilePerThread (optGPS.tilePerThread);
  bool seqTileFlag (optGPS.seqTileFlag);
  bool autoThread (optGPS.autoThread);
  Border<2> border (optGPS.optCrop.size, false);
  const double doubleValue (isnan (optGPS.noDataValue) ?
			    inputImage.getNoDataValue (optGPS.selectedBand) :
			    optGPS.noDataValue);
  if (!isnan (doubleValue) &&
      doubleValue == double (InPixelT (doubleValue))) {
    const InPixelT noDataValue (doubleValue);
    cerr << "noDataValue: " << double (noDataValue) << endl;
    const InPixelT *inputPixels = raster.getPixels ();
    auto start = high_resolution_clock::now ();
    for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
      if (inputPixels [pixelId] == noDataValue)
	border.setBorder (pixelId);
    TreeStats::global.addTime (borderStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }
  GraphWalker<2> graphWalker (border, optGPS.connectivity, optGPS.countingSortCeil);
  ArrayTreeBuilder<InPixelT, InPixelT, 2> atb (raster, graphWalker, treeType, threadPerImage, tilePerThread, seqTileFlag, autoThread, false);

  Tree<2> tree (optGPS.coreCount);
  WeightAttributes<InPixelT, 2> weightAttributes (tree, getDecrFromTreetype (treeType));

  LOG ("builldTree");
  atb.buildTree (tree, weightAttributes);
  LOG ("builldAttributs");
  AreaAttributes<2> areaAttributes (tree);
  BoundingBoxAttributes<2> boundingBoxAttributes (tree);
  RectangularityAttributes<2> rectangularityAttributes (tree, areaAttributes, boundingBoxAttributes);

  LOG ("initValues");
  const DimImgPack *areaValues = areaAttributes.getValues ();
  const InPixelT *weightValues = weightAttributes.getValues ();
  const double *rectValues = rectangularityAttributes.getValues ();

  vector<double> rectScale = optGPS.linearRectBin ?
    linearspace (0., 1., binRectCount) :
    logspace (1./optGPS.maxAreaBin, 1., binRectCount);

  cout << "rectScale: " << endl;
  for (int k=0; k<binRectCount; k++)
	  cout << "   [" << k << "] : " << rectScale[k]+0;
  cout << endl;

  vector<double> areaScale = logspace (1, optGPS.maxAreaBin, binAreaCount);

  vector<double> psTab (binAreaCount * binRectCount, 0.);

  vector<DimBin> comBinIdx (tree.getCompCount ());
  vector<double> compPSVal (tree.getCompCount ());

  const DimParent rootId (tree.getRootId ());
  LOG ("startPS");
  KorriganStats::global.setStart ();

  // XXX check dealThread ok (concurrent access to psTab [comBinIdx[compId]] += volume)
  for (DimParent compId = 0; compId < rootId; ++compId) {
  // dealThread (rootId, optGPS.coreCount,
  //  	      [&areaScale, &rectScale, &weightValues, &tree, &areaValues, &rectValues, &comBinIdx, &compPSVal, &psTab]
  // 	      (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
  // 		for (DimParent compId = minVal; compId < maxVal; ++compId) {
		  DimBin areaBinId = lower_bound (areaScale.begin (), areaScale.end (), double (areaValues[compId])) - areaScale.begin ();
		  DimBin rectBinId = lower_bound (rectScale.begin (), rectScale.end (), double (rectValues[compId])) - rectScale.begin ();
		  double diff = abs ((double)weightValues[compId]-
				     (double)weightValues[tree.getCompParent (compId)]);
		  double volume = diff * areaValues[compId];
		  comBinIdx[compId] = rectBinId + areaBinId * binRectCount;
		  compPSVal[compId] = volume;
		  psTab [comBinIdx[compId]] += volume;
		}
  //	      });
  KorriganStats::global.addTime (PScompute);
  LOG ("endPS");

  string projectionRef;
  vector<double> geolocation;
  inputImage.getGeo (projectionRef, geolocation, optGPS.optCrop.topLeft);

  LOG ("HDF5");
  HDF5 hdf5;
  hdf5.set_compression_level (optGPS.compressLevel);
  hdf5.set_cache_size (optGPS.cacheSize);
  hdf5.open_write (outputFileName);
  tree.save (hdf5);
  hdf5.write_string (projectionRef, "projectionRef", "/" );
  hdf5.write_array (geolocation, "georeference", "/" );
  hdf5.write_array (psTab, "PS", "/" );
  hdf5.write_array (comBinIdx, "compBinIdx", "/");
  hdf5.write_array (compPSVal, "compBinVal", "/");
  hdf5.close_file ();
}

// ========================================
int
main (int argc, char** argv, char** envp) {
  OptGPS optGPS (argc, argv);
  string cropName = "";
  if (optGPS.optCrop.size != optGPS.inputImage.getSize ()) {
    cropName =
      "_"+boost::lexical_cast<string> (optGPS.optCrop.topLeft.coord[0])
      +"x"+boost::lexical_cast<string> (optGPS.optCrop.topLeft.coord[1])
      +"-"+boost::lexical_cast<string> (optGPS.optCrop.size.side[0])
      +"x"+boost::lexical_cast<string> (optGPS.optCrop.size.side[1]);
  }
  string outputFileName  = (boost::filesystem::path (optGPS.outputName) / boost::filesystem::path (optGPS.inputName).replace_extension ("").filename ()).string ()+cropName+"-b"+boost::lexical_cast<std::string> (optGPS.selectedBand)+".kor";

  cout << "fullPS input" << endl
       << " inputImage: " << optGPS.inputImage << endl
       << endl
       << "fullPS output" << endl
       << " precompute: " << outputFileName << endl;

  //cout << GDALGetDataTypeName(optGPS.inputImage.getDataType ()) << endl ; // quelle est l'erreur ?
  switch (optGPS.inputImage.getDataType ()) {
  case GDT_Byte:
    precompute<uint8_t> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_UInt16:
    precompute<uint16_t> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_Int16:
    precompute<int16_t> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_UInt32:
    precompute<uint32_t> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_Int32:
    precompute<int32_t> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_Float32:
    precompute<float> (optGPS.inputImage, outputFileName, optGPS); break;
  case GDT_Float64:
    precompute<double> (optGPS.inputImage, outputFileName, optGPS); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }

  if (optGPS.countFlag)
    cout << TreeStats::global.printDim ();
  if (optGPS.timeFlag) {
    cerr << TreeStats::global.printTime ();
    cerr << KorriganStats::global.printTime ();
  }
  return 0;
}
