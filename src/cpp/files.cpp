////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * Files.cpp
 *
 *  Created on: 11 sept. 2019
 *      Author: more
 */

#include <boost/filesystem.hpp>
#include "files.hpp"

namespace fs = boost::filesystem;


// ----------------------------------------------------------------------
// make a list of input files found in "inputDir" with extension "inputExt"
// ----------------------------------------------------------------------

void		Files::createInputPathList (string inputDir, string inputExt)
{
	fs::path inputDirPath = fs::path(inputDir);
	for (fs::directory_iterator itr(inputDirPath); itr!=fs::directory_iterator(); ++itr)
		if ( (fs::extension(itr->path())).compare("."+inputExt) == 0 )
			inputPathList.push_back ( itr->path() );
}

// ----------------------------------------------------------------------
// make a list of output files in "outputDir" with extension "outputExt"
// creates this list from the list of input files (same filenames)
// ----------------------------------------------------------------------

void		Files::createOutputPathList (string outputDir, string outputExt)
{
	fs::path	outputDirPath = fs::path(outputDir);

	fs::path inputPath, inputFile, outputFile, outputPath;
	for (uint f=0; f<inputPathList.size(); f++)
	{
		inputPath 			= inputPathList[f];
		inputFile				= inputPath.filename();
		outputFile			= inputFile.replace_extension("."+outputExt);
		// operator "/" equals "/" for linux and "\" for windows
		outputPath			= outputDirPath / outputFile;
		outputPathList.push_back (outputPath);
	}
}

// ----------------------------------------------------------------------
// print a list of files (input ou output)
// ----------------------------------------------------------------------

void		Files::printPathList (vector<fs::path> pathList)
{
	cout << "Number of files: " << pathList.size() << endl;

	fs::path path;
	string size;
	for (uint f=0; f<pathList.size(); f++) {
		path	= pathList[f];
		if (!fs::exists(path))
			size	= "0";
		else
			size	= to_string( file_size(path) );
		cout << path.string() << " (" << size << " Bytes) " << endl;
	}
	cout << endl;
}

// ----------------------------------------------------------------------
// print the list of input files
// ----------------------------------------------------------------------

void	Files::printInputPathList ()
{
	printPathList (inputPathList);
}

// ----------------------------------------------------------------------
// print the list of output files
// ----------------------------------------------------------------------

void	Files::printOutputPathList ()
{
	printPathList (outputPathList);
}

// ----------------------------------------------------------------------
// write all the output files (creates empty files)
// ----------------------------------------------------------------------

void	Files::writeOutputPathList ()
{
	fs::path outputPath, outputDir;

	for (uint f=0; f<outputPathList.size(); f++)
	{
		outputPath	= outputPathList[f];
		outputDir		= outputPath.parent_path();
		// if output dir does not exist, then create it
		if (!fs::exists(outputDir)) {
			fs::create_directory(outputDir);
			cout << "  * creating directory: " << outputDir << endl;
		}
		// create an empty file
		fs::ofstream outputFile(outputPath);
	 	cout << " * writing file: " << outputPath << endl;
		outputFile.close();
	}

}

// ----------------------------------------------------------------------
