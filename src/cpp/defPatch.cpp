////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

// ./defPatch --timeFlag ../result/arles_1576x920-1000x1000-b0.kor ../result/ -w 200 -h 200 -x 100 -y 100

#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"

#include "Korrigan.hpp"
#include "KorriganStats.hpp"
#include "OptPatch.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace obelix;
using namespace obelix::triskele;
using namespace obelix::korrigan;

void
defPatch (const string &korInputFileName, const string &patchOutputFileName, const OptPatch &optPatch) {
  DEF_LOG ("defPatch", "korInputFileName:" << korInputFileName << " patchOutputFileName:" << patchOutputFileName);

  Tree<2> tree (optPatch.coreCount);
  vector<double> psTab;
  vector<DimBin> compBinIdx;
  vector<double> compPSVal;
  
  HDF5 hdf5;
  hdf5.open_read (korInputFileName);
  tree.load (hdf5);
  hdf5.read_array (psTab,"PS","/");
  hdf5.read_array (compBinIdx,"compBinIdx","/");
  hdf5.read_array (compPSVal,"compBinVal","/");
  hdf5.close_file ();

  BoundingBoxAttributes<2> boundingBoxAttributes (tree);
  vector<double> patchPSVal (psTab.size ());
  const BoundingBox<2> *bbValues = boundingBoxAttributes.getValues ();

  LOG ("size:" << tree.getSize () << " PS:" << psTab.size () << " compBinIdx:" << compBinIdx.size () << " compPSVal:" << compPSVal.size ());
  DimSideImg
    minX (optPatch.left),
    minY (optPatch.top),
    maxX (optPatch.left+optPatch.width),
    maxY (optPatch.top+optPatch.height);

  const DimParent rootId (tree.getRootId ());
  KorriganStats::global.setStart ();

  // XXX check dealThread ok (concurrent access to patchPSVal [compBinIdx [compId]] += compPSVal [compId])
  for (DimParent compId = 0; compId < rootId; ++compId) {
  // dealThread (rootId, optPatch.coreCount,
  // 	      [&bbValues, &minX, &maxX, &minY, &maxY, &patchPSVal, &compBinIdx, &compPSVal]
  // 	      (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
  // 		for (DimParent compId = minVal; compId < maxVal; ++compId) {
		  const BoundingBox<2> &bbc (bbValues[compId]);
		  if (bbc.min[0] < minX || bbc.max[0] > maxX ||
		      bbc.min[1] < minY || bbc.max[1] > maxY)
		    continue;
		  patchPSVal [compBinIdx [compId]] += compPSVal [compId];
		}
  	      // });
  vector<DimBin> reduceBinIdx;
  vector<double> reduceBinVal;
  for (DimBin binIdx = 0; binIdx < patchPSVal.size (); ++binIdx) {
    if (!patchPSVal[binIdx])
      continue;
    reduceBinIdx.push_back (binIdx);
    reduceBinVal.push_back (patchPSVal[binIdx]);
  }
  KorriganStats::global.addTime (PSfilter);

  vector<DimSideImg> patchSize = {optPatch.width, optPatch.height};
  LOG ("patchSize:" << patchSize[0] << "x" << patchSize[1] << " reduceBinIdx:" << reduceBinIdx.size () << " reduceBinVal:" << reduceBinVal.size ());

  if ((reduceBinIdx.size () * reduceBinVal.size ()) == 0) {
    cout << endl << "Abort : no sub PS !" << endl;
    return;
  }
  
  hdf5.set_compression_level (optPatch.compressLevel);
  hdf5.set_cache_size (optPatch.cacheSize);
  hdf5.open_write (patchOutputFileName);
  hdf5.write_array (patchSize,"patchSize","/" );
  hdf5.write_array (reduceBinIdx,"patchPSBinIdx","/" );
  hdf5.write_array (reduceBinVal,"patchPSBinVal","/" );
  hdf5.close_file ();
}

// ========================================
int
main (int argc, char** argv, char** envp) {
  OptPatch optPatch (argc, argv);
  string outputFileName  =
    (boost::filesystem::path (optPatch.outputName) /
     boost::filesystem::path (optPatch.inputName).replace_extension ("").filename ()).string ()
    +"_"+boost::lexical_cast<string> (optPatch.left)
    +"x"+boost::lexical_cast<string> (optPatch.top)
    +"-"+boost::lexical_cast<string> (optPatch.width)
    +"x"+boost::lexical_cast<string> (optPatch.height)
    +".patch";

  cout << "defPatch input" << endl
       << " korInput: " << optPatch.inputName << endl
       << " crop:" << optPatch.left << "x" << optPatch.top << "[" << optPatch.width << "x" << optPatch.height << "]"<< endl
       << endl
       << "defPatch output" << endl
       << " patchOutput: " << outputFileName << endl;

  defPatch (optPatch.inputName, outputFileName, optPatch);
  if (optPatch.timeFlag) {
    cerr << TreeStats::global.printTime ();
    cerr << KorriganStats::global.printTime ();
  }
  return 0;
}
