////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string.h>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "misc.hpp"
#include "Appli/OptCropParser.hpp"

#include "OptGPS.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;
using namespace obelix::korrigan;

namespace po = boost::program_options;


static const DimImg MAX_AREA_BIN = 100000000;

// ================================================================================
static OptCropParser optCropParser;
OptGPS::OptGPS ()
  : OptBase (),
    gpsDescription ("GPS options"),
    selectedBand (0),
    noDataValue (NAN),
    countFlag (false),
    countingSortCeil (2),
    connectivity (Connectivity (Connectivity::C4|Connectivity::CT)),
    autoThread (false),
    threadPerImage (max (DimCore (1), coreCount/4)),
    tilePerThread (max (DimCore (1), threadPerImage*2)),
    seqTileFlag (false),
    maxAreaBin (MAX_AREA_BIN),
    linearRectBin (false),
    compressLevel (5),
    cacheSize (1024*1024) {

  gpsDescription.add_options ()
    ("band,b", po::value<int> (&selectedBand)->default_value (selectedBand), "input band")
    ("noDataValue", po::value<double> (&noDataValue)->default_value (noDataValue), "value use as nodata")
    ("countFlag", po::bool_switch (&countFlag), "give pixels and nodes count")
    ("countingSortCeil", po::value<unsigned int> (&countingSortCeilLocal)->default_value (countingSortCeilLocal), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")
    ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
    ("autoThread", po::bool_switch (&autoThread), "auto tune tilePerThread count")
    ("threadPerImage", po::value<DimCore> (&threadPerImage)->default_value (threadPerImage), "thread per Image to build tree")
    ("tilePerThread", po::value<DimCore> (&tilePerThread)->default_value (tilePerThread), "tiles per threads")
    ("seqTileFlag", po::bool_switch (&seqTileFlag), "force Tile sequential computation")
    ("maxAreaBin", po::value<DimImg> (&maxAreaBin)->default_value (maxAreaBin), "max pixel for area bin scale")
    ("linearRectBin", po::bool_switch (&linearRectBin), "use linear scale for rectangularity bin (rather than log)")
    ("compressLevel", po::value<unsigned int> (&compressLevel)->default_value (compressLevel), "HDF5 compression level for korrigan file")
    ("cacheSize", po::value<size_t> (&cacheSize)->default_value (cacheSize), "HDF5 chunk cache size")
    ;

  allDescription
    .add (optCropParser.description)
    .add (gpsDescription);
}

OptGPS::OptGPS (int argc, char** argv)
  : OptGPS () {
  parse (argc, argv);
}

// ================================================================================
void
OptGPS::extendedUsage () const {
  cout << optCropParser.description << gpsDescription;
}

void
OptGPS::parse (int argc, char** argv) {
  OptBase::parse (argc, argv);

  countingSortCeil = countingSortCeilLocal;
  inputImage.setFileName (inputName);
  inputImage.readImage ();
  optCropParser.parse (optCrop, inputImage.getSize (), parsed);
  if (selectedBand >= inputImage.getBandCount ())
    throw invalid_argument (string ("Can't select band")+boost::lexical_cast<std::string> (selectedBand)+" (only "+boost::lexical_cast<std::string> (inputImage.getBandCount ())+")!");
}
