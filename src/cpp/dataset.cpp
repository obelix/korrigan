////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * dataset.cpp
 *
 *  Created on: 23 sept. 2019
 *      Author: more
 */

#include <boost/filesystem.hpp>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <sstream>

#include "dataset.hpp"

using namespace std;
namespace fs=boost::filesystem;


// ======================================================================
// ======================================================================


PointPixel::PointPixel (const int x, const int y)
  : x (x), y (y)
{
}

// ======================================================================

SizePixel::SizePixel (const int width, const int height)
: width (width), height (height)
{
}

// ======================================================================

ZonePixel::ZonePixel (const int x1, const int y1, const int x2, const int y2)
: x1 (x1), y1 (y1), x2 (x2), y2 (y2)
{
}

// ======================================================================

ZonePixel::ZonePixel (const SizePixel sizePixel)
{
	x1	= 0;
	y1	= 0;
//	x2	= sizePixel.width - 1;
//	y2	= sizePixel.height - 1;
	x2	= sizePixel.width;
	y2	= sizePixel.height;
}

// ======================================================================

int ZonePixel::width()
{
	return (x2-x1);
}

// ======================================================================

int ZonePixel::height()
{
	return (y2-y1);
}

// ======================================================================

PointGeo::PointGeo (const double x, const double y)
  : x (x), y (y)
{
}

// ======================================================================

SizeGeo::SizeGeo (const double width, const double height)
: width (width), height (height)
{
}

// ======================================================================

ZoneGeo::ZoneGeo (const double x1, const double y1, const double x2, const double y2)
: x1 (x1), y1 (y1), x2 (x2), y2 (y2)
{
}

// ======================================================================

double ZoneGeo::width()
{
	return (x2-x1);
}

// ======================================================================

double ZoneGeo::height()
{
	return (y2-y1);
}


// ======================================================================
// ======================================================================
// ======================================================================
// ======================================================================


//// ======================================================================
//
//OGRSpatialReference		CRS::GetSpatialRef ()
//{
//	OGRSpatialReference spatialRef;
//	char const * szWkt = gdalDataset->GetProjectionRef();
//  spatialRef.importFromWkt(&szWkt);
//  return spatialRef;
//};

// ======================================================================

OGRSpatialReference		gdalDataset2SpatialRef (GDALDataset *gdalDataset)
{
	OGRSpatialReference spatialRef;
	char const * szWkt = gdalDataset->GetProjectionRef();
  spatialRef.importFromWkt(&szWkt);
  return spatialRef;
};


// ======================================================================
// constructor

CRS::CRS (GDALDataset *gdalDataset)
{
	this->gdalDataset		= gdalDataset;
  projectionRef 			= gdalDataset->GetProjectionRef ();

  double gtDefault[]	= {0.,1.,0.,0.,0.,1.};
  geoTransform.assign(gtDefault, gtDefault+6);
  CPLErr err 					= gdalDataset->GetGeoTransform (&geoTransform[0]);

//  char const * szWkt = gdalDataset->GetProjectionRef();
//  spatialRef.importFromWkt(&szWkt);
	spatialRef					= gdalDataset2SpatialRef (gdalDataset);
}

// ======================================================================
// convert a point (X,Y) from pixel coordinates to geographical coordinates
// for this it uses the gdal geotransform vector of the class

PointGeo CRS::pixelToGeo (PointPixel pointPixel)
{
	double geoXOrigin			= geoTransform [0];
	double geoYOrigin			= geoTransform [3];
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	PointGeo pointGeo;
	pointGeo.x	= geoXOrigin + geoXPixelsize * pointPixel.x;
	pointGeo.y	= geoYOrigin + geoYPixelsize * pointPixel.y;
	return pointGeo;
}

// ======================================================================
// convert a size (width,height) from pixel coordinates to geographical coordinates
// for this it uses the gdal geotransform vector of the class

SizeGeo CRS::pixelToGeo (SizePixel sizePixel)
{
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	SizeGeo sizeGeo;
	// note: pixel size can be negative, so for size function we use absolute value
	sizeGeo.width		= abs( geoXPixelsize * sizePixel.width );
	sizeGeo.height	= abs( geoYPixelsize * sizePixel.height );
	return sizeGeo;
}

// ======================================================================
// convert a rectangular zone (x1,y1,x2,y2) from pixel coordinates to geographical coordinates
// for this it uses the gdal geotransform vector of the class

ZoneGeo CRS::pixelToGeo (ZonePixel zonePixel)
{
	double geoXOrigin			= geoTransform [0];
	double geoYOrigin			= geoTransform [3];
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	ZoneGeo zoneGeo;
	// usual conversion (without order)
	double x1Tmp	= geoXOrigin + geoXPixelsize * zonePixel.x1;
	double y1Tmp	= geoYOrigin + geoYPixelsize * zonePixel.y1;
	double x2Tmp	= geoXOrigin + geoXPixelsize * zonePixel.x2;
	double y2Tmp	= geoYOrigin + geoYPixelsize * zonePixel.y2;
	// reorder coordinates so that x1<=x2 and y1<=y2
	zoneGeo.x1	= min(x1Tmp, x2Tmp);
	zoneGeo.y1	= min(y1Tmp, y2Tmp);
	zoneGeo.x2	= max(x1Tmp, x2Tmp);
	zoneGeo.y2	= max(y1Tmp, y2Tmp);
	return zoneGeo;
}

// ======================================================================
// convert a point (X,Y) from geographical coordinates to pixel coordinates
// for this it uses the gdal geotransform vector of the class

PointPixel CRS::geoToPixel (PointGeo pointGeo)
{
	double geoXOrigin			= geoTransform [0];
	double geoYOrigin			= geoTransform [3];
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	// note: round to nearest integer to make a better conversion
	PointPixel pointPixel;
	pointPixel.x	= lround ( (pointGeo.x - geoXOrigin) / geoXPixelsize );
	pointPixel.y	= lround ( (pointGeo.y - geoYOrigin) / geoYPixelsize );
	return pointPixel;
}

// ======================================================================

SizePixel CRS::geoToPixel (SizeGeo sizeGeo)
{
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	// note: pixel size can be negative, so for size function we use absolute value
	// note: round to nearest integer to make a better conversion
	SizePixel sizePixel;
	sizePixel.width		= lround ( abs( sizeGeo.width / geoXPixelsize ) );
	sizePixel.height	= lround ( abs( sizeGeo.height / geoYPixelsize ) );
	return sizePixel;
}

// ======================================================================

ZonePixel CRS::geoToPixel (ZoneGeo zoneGeo)
{
	double geoXOrigin			= geoTransform [0];
	double geoYOrigin			= geoTransform [3];
	double geoXPixelsize	= geoTransform [1];
	double geoYPixelsize	= geoTransform [5];

	// note: round to nearest integer to make a better conversion
	ZonePixel zonePixel;
	// usual conversion (without order)
	int x1Tmp	= lround ( (zoneGeo.x1 - geoXOrigin) / geoXPixelsize );
	int y1Tmp	= lround ( (zoneGeo.y1 - geoYOrigin) / geoYPixelsize );
	int x2Tmp	= lround ( (zoneGeo.x2 - geoXOrigin) / geoXPixelsize );
	int y2Tmp	= lround ( (zoneGeo.y2 - geoYOrigin) / geoYPixelsize );
	// reorder coordinates so that x1<=x2 and y1<=y2
	zonePixel.x1	= min(x1Tmp, x2Tmp);
	zonePixel.y1	= min(y1Tmp, y2Tmp);
	zonePixel.x2	= max(x1Tmp, x2Tmp);
	zonePixel.y2	= max(y1Tmp, y2Tmp);
	return zonePixel;
}

// ======================================================================

int		CRS::getEPSG ()
{
	const char* EPSGname =  spatialRef.GetAuthorityCode("PROJCS");
  int EPSG	= atoi(EPSGname);
  return EPSG;
}

// ======================================================================

double	CRS::getNoDataValue (int band)
{
	GDALRasterBand &poBand		= *gdalDataset->GetRasterBand (band+1);
	double noDataValue 				= poBand.GetNoDataValue ();
	return (noDataValue);
}

// ======================================================================

//// needs gdal version 2.2 or later !!!
//
//bool		CRS::containNoData (int band, ZonePixel zonePixel)
//{
//	GDALRasterBand &poBand	= *gdalDataset->GetRasterBand (band+1);
//	bool testNoData 				= poBand.GetDataCoverageStatus (zonePixel.x1, zonePixel.x2, zonePixel.width(), zonePixel.height());
//	return (testNoData);
//}



//// ======================================================================
//// ======================================================================
//
//
//			Image::Image (string path)
//{
//	this->path	= path;
//}
//
//// ======================================================================
//
//void 	Image::readInfo ()
//{
//	gdalDataset							= (GDALDataset *) GDALOpen (path.c_str (), GA_ReadOnly);
//	// image dimensions
//  size.width							= gdalDataset->GetRasterXSize ();
//  size.height							= gdalDataset->GetRasterYSize ();
//	bandCount								= gdalDataset->GetRasterCount ();
//
//	int band=0;
//	GDALRasterBand &poBand	= *gdalDataset->GetRasterBand (band+1);
//	noDataValue 						= poBand.GetNoDataValue ();
//
//	cout << "path = " << path << endl;
//	cout << "size.width = " << size.width << endl;
//	cout << "size.height = " <<  size.height << endl;
//	cout << "bandCount = " << bandCount << endl;
//	cout << "noDataValue = " << noDataValue << endl;
//}
//
//// ======================================================================
//
//void	Image::readBand (int band)
//{
//	readInfo();
//
//	// WARNING : until now "pixels" has a size of 0!!!
//	long totalNbPixels = size.width * size.height * bandCount;
//	pixels.resize (totalNbPixels);
//
//	PixelT *startPixelsBandB = &pixels [ size.width * size.height * (band-1) ];
//
//	// !!! GDAL bands start at 1 (not 0)
//	GDALRasterBand &poBand = *gdalDataset->GetRasterBand (band);
//	GDALDataType bandType = poBand.GetRasterDataType ();
//
//	CPLErr err = poBand.RasterIO (GF_Read, 0, 0, size.width, size.height,
//			startPixelsBandB, size.width, size.height, bandType, 0, 0);
//
//	if (err != CE_None)
//		cout << "Image::readTiffBand: can't read band " << band << " from image: "<< endl << path << endl;
//
//	PixelT	minValue	= *min_element(begin(pixels), end(pixels));
//	PixelT	maxValue	= *max_element(begin(pixels), end(pixels));
//
//	cout << "pixels.size() = " << pixels.size() << endl;
//	cout << "minValue = " << minValue << endl;
//	cout << "maxValue = " << maxValue << endl;
//}






// ======================================================================
// get tif file dimensions (using bash!!!)
// does not work : impossible to collect (easily!) the ouput of bash
//void Dataset::getImDimensionsUsingBash()
//{
//	string bashCommand;
//
//	bashCommand = "identify " + imagePath + " 2>&1 | head -n 1 | grep x | cut -f 3 -d' ' | cut -f 1 -d'x'";
//	system(("bash -c \"" + bashCommand + "\"").c_str());
//	bashCommand = "identify " + imagePath + " 2>&1 | head -n 1 | grep x | cut -f 3 -d' ' | cut -f 2 -d'x'";
//	system(("bash -c \"" + bashCommand + "\"").c_str());
//}

// ======================================================================
// get image dimensions by reading file metadata
// cf code in /korrigan/thirdparty/triskele/src/IImage.cpp

void			Dataset::getImageAndGtInfo ()
{
	// image dimensions
	string	imagePath			= imageDir+"/"+imageFile;
  gdalImageDataset 			= (GDALDataset *) GDALOpen (imagePath.c_str (), GA_ReadOnly);
  imageBandcount				= gdalImageDataset->GetRasterCount ();
  imageSizePixel.width	= gdalImageDataset->GetRasterXSize ();
  imageSizePixel.height	= gdalImageDataset->GetRasterYSize ();

//	// image projection and coordinate system
//  imageProjectionRef 		= gdalImageDataset->GetProjectionRef ();
//  double gtDefault[] 		= {0.,1.,0.,0.,0.,1.};
//  imageGeoTransform.assign(gtDefault, gtDefault+6);
//  CPLErr err 						= gdalImageDataset->GetGeoTransform (&imageGeoTransform[0]);
  CRS imageCrs(gdalImageDataset);
  imageZoneGeo					= imageCrs.pixelToGeo( ZonePixel(imageSizePixel) );
  imageEPSG							= imageCrs.getEPSG();
	noDataValue						=	imageCrs.getNoDataValue();

	// ----------------------------------------------------------------------

  if ( gtDir!="" && gtFile!="" )
	{
		// image dimensions
		string	gtPath			= gtDir+"/"+gtFile;
		gdalGtDataset 			= (GDALDataset *) GDALOpen (gtPath.c_str (), GA_ReadOnly);
		gtBandcount					= gdalGtDataset->GetRasterCount ();
		gtSizePixel.width		= gdalGtDataset->GetRasterXSize ();
		gtSizePixel.height	= gdalGtDataset->GetRasterYSize ();

//		// ground truth projection and coordinate system
//		gtProjectionRef 		= gdalGtDataset->GetProjectionRef ();
//		double gtDefault[] 	= {0.,1.,0.,0.,0.,1.};
//		gtGeoTransform.assign(gtDefault, gtDefault+6);
//		CPLErr err 					= gdalGtDataset->GetGeoTransform (&gtGeoTransform[0]);

	  CRS gtCrs(gdalGtDataset);
	  gtZoneGeo						= gtCrs.pixelToGeo( ZonePixel(gtSizePixel) );
	  gtEPSG							= gtCrs.getEPSG();
	}

}

// ======================================================================
// create patch file name from patch file pattern

string		Dataset::getPatchFile (int band, string outputPrefixStr, int indX, int indY, ZonePixel patchZonePixel, int GTClass, float maintclassTh)
{
	// convert patch indices to string (with 0 padding):
	int nZeroPad=4;
	ostringstream indXStream, indYStream;
	indXStream << std::setw(nZeroPad) << std::setfill('0') << indX;
	indYStream << std::setw(nZeroPad) << std::setfill('0') << indY;
	string indXStr = indXStream.str(), indYStr = indYStream.str();
//	cout << "indXStr = " << indexStr << "." << endl;

	// conversion numbers -> string
  string bandStr				= to_string( band );
	string patchX1Str			= to_string( patchZonePixel.x1 );
	string patchY1Str			= to_string( patchZonePixel.y1 );
	string patchX2Str			= to_string( patchZonePixel.x2 );
	string patchY2Str			= to_string( patchZonePixel.y2 );
	string gtClassStr			= to_string( GTClass ) + "=" + classIndexToClassname(GTClass);
	string mainclassThStr	= to_string( lround(maintclassTh*100.) ) + "%";

	// conversion numbers -> string
	string patchFile			= patchFilePattern;
	boost::replace_all(patchFile, "<prefix>", outputPrefixStr);
	boost::replace_all(patchFile, "<indX>", indXStr);
	boost::replace_all(patchFile, "<indY>", indYStr);
	boost::replace_all(patchFile, "<band>", bandStr);
	boost::replace_all(patchFile, "<patchX1>", patchX1Str);
	boost::replace_all(patchFile, "<patchY1>", patchY1Str);
	boost::replace_all(patchFile, "<patchX2>", patchX2Str);
	boost::replace_all(patchFile, "<patchY2>", patchY2Str);
	boost::replace_all(patchFile, "<gtclass>", gtClassStr);
	boost::replace_all(patchFile, "<mainclassth>", mainclassThStr);

	return (patchFile);
}

// ======================================================================
// crop image (use a call to triskele to do it)

void			Dataset::cropImageUsingTriskele (string inputPath, string outputPath, int band, ZonePixel patchZonePixel)
{
	string triskeleProgram = "./thirdparty/triskele/channelGenerator";
	if (!fs::exists(triskeleProgram))
		cout << " *** ERROR *** program not found:" << endl << triskeleProgram << endl;

	// ----------------------------------------------------------------------
	string bandStr				= to_string(band);

	// ----------------------------------------------------------------------
	string cropXStr	= to_string( patchZonePixel.x1 );
	string cropYStr	= to_string( patchZonePixel.y1 );
	string cropWStr	= to_string( abs( patchZonePixel.x2-patchZonePixel.x1)+1 );
	string cropHStr	= to_string( abs( patchZonePixel.y2-patchZonePixel.y1)+1 );

	// ----------------------------------------------------------------------

	string bashCommand = triskeleProgram + " " + inputPath + " " + outputPath + " -c "+ bandStr
			+ " -x " + cropXStr + " -y " + cropYStr + " -w " + cropWStr + " -h " + cropHStr + " > /dev/null";
//	cout << bashCommand << endl;
	system(("bash -c \"" + bashCommand + "\"").c_str());
}

// ======================================================================

void 		Dataset::cropImageUsingTriskele (string inputPath, string outputPath, int band, ZoneGeo patchZoneGeo, GDALDataset* gdalDataset)
{
	CRS crs(gdalDataset);
	ZonePixel patchZonePixel = crs.geoToPixel(patchZoneGeo);
	cropImageUsingTriskele (inputPath, outputPath, band, patchZonePixel);
}

// ======================================================================

template<typename PixelT>
void		Dataset::readPixels (string path, int band)
{
	// open file
	GDALDataset *gdalDataset	= (GDALDataset*) GDALOpen (path.c_str (), GA_ReadOnly);

	// find dimensions
	SizePixel sizePixel				= SizePixel ( gdalDataset->GetRasterXSize(), gdalDataset->GetRasterYSize() );
	long nPixels							= sizePixel.width * sizePixel.height;
	int bandCount							= gdalDataset->GetRasterCount();

	// read raster values
	GDALRasterBand &poBand		= *gdalDataset->GetRasterBand (band+1);
	GDALDataType bandType			= poBand.GetRasterDataType ();
//	vector<PixelT> pixelsAllBands (sizePixel.width * sizePixel.height * bandCount);
//	PixelT *startPixel1Band 	= &pixelsAllBands [ sizePixel.width * sizePixel.height * band ];
	vector<PixelT> pixels (sizePixel.width * sizePixel.height);

	CPLErr err = poBand.RasterIO (GF_Read, 0, 0, sizePixel.width, sizePixel.height,
																&pixels[0], sizePixel.width, sizePixel.height, bandType, 0, 0);
	if (err != CE_None)
		cout << "[Dataset::getHistogram] : can't read band " << band << " from image: "<< endl << path << endl;


	PixelT minValue	= *min_element(begin(pixels), end(pixels));
	PixelT maxValue	= *max_element(begin(pixels), end(pixels));
	double meanValue = accumulate( pixels.begin(), pixels.end(), 0.0) / pixels.size();
	double minMaxValues[2];
	poBand.ComputeRasterMinMax(FALSE, minMaxValues);

	cout << endl;
	cout << "path = " << path << endl;
	cout << "band = " << band << endl;
	cout << "pixels.size() = " << pixels.size() << endl;
	cout << "minValue = " << +minValue << endl;
	cout << "maxValue = " << +maxValue << endl;
	cout << "minMaxValues = " << minMaxValues[0] << ", " << minMaxValues[1] << endl;
	cout << "meanValue = " << meanValue << endl;
	cout << "pixels[k] = "<< endl;
	for (int k=0; k<20; k++)
		cout << "[" << k << "]:" << +pixels[k] << " ";
	cout << endl;
}


// ======================================================================

vector<float>		Dataset::getNormHistogram (string path, int band)
{
	// assumed pixel type for histograms
	typedef uint8_t	HistogramPixelT;

	// open file
	GDALDataset *gdalDataset	= (GDALDataset*) GDALOpen (path.c_str (), GA_ReadOnly);

	// find dimensions
	SizePixel sizePixel				= SizePixel ( gdalDataset->GetRasterXSize(), gdalDataset->GetRasterYSize() );
	long nPixels							= sizePixel.width * sizePixel.height;
	int bandCount							= gdalDataset->GetRasterCount();

	// read raster values
	GDALRasterBand &poBand		= *gdalDataset->GetRasterBand (band+1);
	GDALDataType bandType			= poBand.GetRasterDataType ();
	vector<HistogramPixelT> pixelsAllBands (sizePixel.width * sizePixel.height * bandCount);
	HistogramPixelT *startPixel1Band 	= &pixelsAllBands [ sizePixel.width * sizePixel.height * band ];
	CPLErr err = poBand.RasterIO (GF_Read, 0, 0, sizePixel.width, sizePixel.height,
																startPixel1Band, sizePixel.width, sizePixel.height, bandType, 0, 0);
	if (err != CE_None)
		cout << "[Dataset::getHistogram] : can't read band " << band << " from image: "<< endl << path << endl;

	// extract only pixels from 1 band
	vector<HistogramPixelT>   pixels (startPixel1Band, startPixel1Band + nPixels);

	HistogramPixelT minValue	= *min_element(begin(pixels), end(pixels));
	HistogramPixelT maxValue	= *max_element(begin(pixels), end(pixels));
	HistogramPixelT nBin			= maxValue + 1;
//	cout << " min = " << +minValue << " max = " << +maxValue << "   nBin: " << +nBin << endl;




//	if (path.find("patchGTTmp") == string::npos)
	{
		cout << "path = " << path << endl;
		cout << "pixels.size() = " << pixels.size() << endl;
	}





	// usual histogram
	vector<long>	intHistogram (nBin, 0);
	long xy;
//	cout << " sizePixel.height = " << sizePixel.height << " sizePixel.width = " << sizePixel.width << endl;
//	cout << " pixels.size() = " << pixels.size() << endl;
	for (int y=0; y<sizePixel.height; y++)
		for (int x=0; x<sizePixel.width; x++)
			{
				xy = x + sizePixel.width * y;
				intHistogram [ pixels[xy] ] ++;
			}

	// normalized histogram
	vector<float>	normHistogram (nBin);
	for (int b=0; b<nBin; b++)
		normHistogram [b] = (float)(intHistogram[b]) / (float)nPixels;

//	for (int b=0; b<nBin; b++)
//		if (normHistogram[b])
//		cout << "normHistogram[" << b << "] = " << normHistogram[b] << endl;

	return normHistogram;
}

// ======================================================================

int			Dataset::getMainClass (string path, int band, float threshold)
{
	// get histogram
	vector<float> normHistogram = getNormHistogram(path, band);

	// get the position of main class (=with max value)
	int maxClass	= max_element(begin(normHistogram), end(normHistogram)) - begin(normHistogram);

	int mainClass=0;
	if (normHistogram[maxClass] >= threshold)
		mainClass	= maxClass;

//	cout << "maxClass = " << maxClass << "   mainClass = " << mainClass << endl;

	return mainClass;
}

// ======================================================================
// http://osr-cesbio.ups-tlse.fr/~oso/nomenclature_ocs_cesbio.txt
//
//	culture ete:11
//	culture hiver:12
//	foret feuillus:31			// hardwood forests
//	foret coniferes:32		// coniferous forests
//	pelouses:34
//	landes ligneuses:36
//	urbain dense:41
//	urbain diffus:42
//	zones ind et com:43
//	surfaces routes:44
//	surfaces minerales:45
//	plages et dunes:46
//	eau:51
//	glaciers ou neige: 53
//	prairies:211
//	vergers:221
//	vignes:222

string		Dataset::classIndexToClassname (int classIndex)
{
	vector<string> classNames (256);
	classNames [  0] = "inconnu";

	classNames [ 11] = "culture_ete";
	classNames [ 12] = "culture_hiver";
	classNames [ 31] = "foret_feuillus";
	classNames [ 32] = "foret_coniferes";
	classNames [ 34] = "pelouses";
	classNames [ 36] = "landes_ligneuses";
	classNames [ 41] = "urbain_dense";
	classNames [ 42] = "urbain_diffus";
	classNames [ 43] = "zones_ind_et_com";
	classNames [ 44] = "surfaces_routes";
	classNames [ 45] = "surfaces_minerales";
	classNames [ 46] = "plages_et_dunes";
	classNames [ 51] = "eau";
	classNames [ 53] = "glaciers ou neige";
	classNames [211] = "prairies";
	classNames [221] = "vergers";
	classNames [222] = "vignes";

	return classNames [classIndex];
}


// ======================================================================
// constructor

			Dataset::Dataset()
{
	// *** IMPORTANT *** needed before using any gdal command
	GDALAllRegister ();
}


// ======================================================================
// initiate values with user parameters

void			Dataset::Init(string imageDir, string imageFile,
												string gtDir, string gtFile)
{
	// ----------------------------------------------------------------------
	// Initialization of members for image

	this->imageDir	= imageDir;
  this->imageFile	= imageFile;
	this->gtDir			= gtDir;
  this->gtFile		= gtFile;

  cout << "Image File: " << imageFile << endl << endl;

	// ----------------------------------------------------------------------

	string		imagePath	= imageDir+"/"+imageFile;

	// ----------------------------------------------------------------------
	// get image and ground truth (if present) information from files (dimensions, coord system, ...)

	getImageAndGtInfo();

	// ----------------------------------------------------------------------
	// prepare patch file pattern

	string imageFileNoext	= fs::change_extension(imageFile, "").string();
	string imageExt				= fs::path(imageFile).extension().string();
	patchFilePattern			= imageFileNoext + "_<prefix>=<indX>,<indY>_band=<band>_x1=<patchX1>_y1=<patchY1>_x2=<patchX2>_y2=<patchY2>_GTclass=<gtclass>_<mainclassth>"+imageExt;

	// ----------------------------------------------------------------------
	patchCoord = "";
}

// ======================================================================
// create the dataset

void			Dataset::CreatePatches (string patchDir, SizeGeo patchSizeGeo, float patchOverlap, float mainclassTh)
{
	// number of bands considered for creating the patches
//	int patchBandcount=imageBandcount;
	int patchBandcount=1;

	// if direct call to this function, patch coordinates are pixels (otherwize geo)
	if (patchCoord == "")
		patchCoord = "meters";

	// ----------------------------------------------------------------------
  // sign correction (if necessary) for patch "size": "width" must be (+) and height must be (-)

  patchSizeGeo.width	= abs (patchSizeGeo.width) * 1.0;
  patchSizeGeo.height	= abs (patchSizeGeo.height) * -1.0;

	// ----------------------------------------------------------------------
	// Initialization of members for patch

  this->patchDir			= patchDir;
  this->patchSizeGeo	= patchSizeGeo;
  this->patchOverlap	= patchOverlap;
  this->patchCoord		= patchCoord;

	string patchGTDir	= patchDir + "/" + "patchGT";

	// ----------------------------------------------------------------------

	if (!fs::exists(patchDir))
		fs::create_directory(fs::path(patchDir));
	if (!fs::exists(patchGTDir))
		fs::create_directory(fs::path(patchGTDir));
	// ----------------------------------------------------------------------
  // converts patch size from geographical coordinates to pixel coordinates
  CRS				imageCrs(gdalImageDataset);
  CRS				gtCrs(gdalGtDataset);


	patchSizePixel			= imageCrs.geoToPixel (patchSizeGeo);

	// ----------------------------------------------------------------------

	// patch steps (in x and y)
	SizeGeo patchStepGeo;
	patchStepGeo.width	= lround( patchSizeGeo.width * patchOverlap );
	patchStepGeo.height	= lround( patchSizeGeo.height * patchOverlap );
	SizePixel patchStepPixel = imageCrs.geoToPixel(patchStepGeo);

	// number of patches in X, in Y and in total (X*Y*nbbands)
	// count the number of possible patch centers (knowing the patch step) in a region of the image
	// which excludes 2 margins of 1/2 patch size (so region size = image size - patch size)
	// note: abs() is to avoid any problem with signs of width and height
	int nPatchX	= floor( ( abs(imageZoneGeo.width()) - abs(patchSizeGeo.width) ) / abs(patchStepGeo.width) ) + 1;
	int nPatchY	= floor( ( abs(imageZoneGeo.height()) - abs(patchSizeGeo.height) ) / abs(patchStepGeo.height) ) + 1;
	int nPatch	= nPatchX*nPatchY*patchBandcount;

	// patch origin (=top-left) for first patch
	PointGeo patchOriginGeoFirst;
	patchOriginGeoFirst.x =	imageZoneGeo.x1;
	patchOriginGeoFirst.y =	imageZoneGeo.y2;

//	// patch origin (=top-left) for last patch
//	PointGeo patchOriginGeoLast;
//	patchOriginGeoLast.x	=	patchOriginGeoFirst.x + patchStepGeo.width * nPatchX;
//	patchOriginGeoLast.y =	patchOriginGeoFirst.y + patchStepGeo.height * nPatchY;

	// ----------------------------------------------------------------------

	cout << "IMAGE: " << endl;
	cout << "_ file: " << imageFile << endl;
	cout << fixed << setprecision(3);
	cout << "_ dimensions: (" << imageZoneGeo.width() << " x " << imageZoneGeo.height() <<  ") " << patchCoord << endl;
	cout << "_           : (" << imageSizePixel.width << " x " << imageSizePixel.height <<  ") pixels, nbands=" << imageBandcount << endl;
	cout << "_ rectangular zone (x1;y1;x2;y2) (using coord. system: EPSG:" << imageEPSG << "): " << endl;
	cout << "  (" << imageZoneGeo.x1 << ", " << imageZoneGeo.y1 << ", " << imageZoneGeo.x2 << ", " << imageZoneGeo.y2 << ") in " << patchCoord<< endl;
	cout << "_ No-Data Value = " << noDataValue << endl;
	cout << endl;

	cout << "GROUND TRUTH: " << endl;
	cout << "_ file: " << gtFile << endl;
	cout << "_ dimensions: (" << gtZoneGeo.width() << " x " << gtZoneGeo.height() <<  ") " << patchCoord << endl;
	cout << "_           : (" << gtSizePixel.width << " x " << gtSizePixel.height <<  ") pixels, nbands=" << gtBandcount << endl;
	cout << "_ rectangular zone (x1;y1;x2;y2) (using coord. system: EPSG:" << gtEPSG << "): " << endl;
	cout << "  (" << fixed << gtZoneGeo.x1 << ", " << gtZoneGeo.y1 << ", " << gtZoneGeo.x2 << ", " << gtZoneGeo.y2 << ") in " << patchCoord << endl;
	cout << "_ main class proportion threshold (to keep patch) : " << mainclassTh*100 << " % " << endl;
	cout << endl;

	cout << "PATCHES: " << endl;
	cout << "_ file pattern: " << patchFilePattern << endl;
	cout << "_ dimensions: (" << showpos << patchSizeGeo.width << " x " << patchSizeGeo.height << noshowpos << ") " << patchCoord << endl;
	cout << "_           : (" << showpos << patchSizePixel.width << " x " << patchSizePixel.height << noshowpos << ") pixels, nbands=" << patchBandcount << endl;
	cout << "_ steps: (" << showpos << patchStepGeo.width << ", " << patchStepGeo.height << noshowpos << ") meters, overlap=" << patchOverlap << endl;
	cout << "_      : (" << showpos << patchStepPixel.width << ", " << patchStepPixel.height << noshowpos << ") pixels, overlap=" << patchOverlap << endl;
	cout << "_ number of patches = " << nPatchX << " x " << nPatchY << " x " << patchBandcount << " bands = " << nPatch << endl;
	cout << endl;

	// ----------------------------------------------------------------------

	int 			band, patchY, patchX;
	long 			patch=0;
	PointGeo	patchOriginGeo, patchOppositeGeo;
	ZoneGeo		patchZoneGeo;
	ZonePixel	patchZonePixel, gtZonePixel;
	string		patchFile, patchPath, patchGTFile, patchGTPath;
	string		imagePath	= imageDir+"/"+imageFile;
	string		gtPath		= gtDir+"/"+gtFile;
	string		patchGTFileTmp = "patchGTTmp.tif";
	string		patchGTPathTmp	= patchGTDir+"/"+patchGTFileTmp;

//	readPixels<uint16_t>(imagePath,0);
//	readPixels<uint8_t>(imagePath,0);
	readPixels<int16_t>(imagePath,0);

	exit(0);


//	patchOriginGeo	= patchOriginGeoFirst;
//		for (patchOriginGeo.y=patchOriginGeoFirst.y; patchOriginGeo.y<=patchOriginGeoLast.y; patchOriginGeo.y+=patchStepGeo.height)
//			for (patchOriginGeo.x=patchOriginGeoFirst.x; patchOriginGeo.x<=patchOriginGeoLast.x; patchOriginGeo.x+=patchStepGeo.width)


	for (band=0; band<patchBandcount; band++)
	{
		cout << "   -> BAND " << band << " : " << endl;
		cout << endl;

		for (patchY=0; patchY<nPatchY; patchY++)
		{
			for (patchX=0; patchX<nPatchX; patchX++)
			{
				// defines current origin point and current opposite point
				patchOriginGeo.x		= patchOriginGeoFirst.x + patchX * patchStepGeo.width;
				patchOriginGeo.y		= patchOriginGeoFirst.y + patchY * patchStepGeo.height;
				patchOppositeGeo.x	= patchOriginGeo.x + patchSizeGeo.width;
				patchOppositeGeo.y	= patchOriginGeo.y + patchSizeGeo.height;
				// deduces patch zone
				patchZoneGeo 				= ZoneGeo(patchOriginGeo.x, patchOppositeGeo.y, patchOppositeGeo.x, patchOriginGeo.y);
				patchZonePixel			= imageCrs.geoToPixel(patchZoneGeo);

				// defines corresponding ground truth zone (using projection between 2 different spatial references)
				gtZoneGeo						= projectZoneGeo(patchZoneGeo, gdalImageDataset, gdalGtDataset);
				gtZonePixel					= gtCrs.geoToPixel(gtZoneGeo);

				// ----------------------------------------------------------------------

				// fake crop just to determine the main GT class
				cropImageUsingTriskele (gtPath, patchGTPathTmp, 0, gtZoneGeo, gdalGtDataset);

				// get the main class from GT patch image
				int mainGTClass = getMainClass (patchGTPathTmp, 0, mainclassTh);

				if (mainGTClass != 0)
				{
					cout << "Patch " << patch << " / " << nPatch << " = (" << patchX << "," << patchY << ") :" << endl;
					cout << " (x1;y1;x2;y2) = (" << patchZoneGeo.x1 << "; " << patchZoneGeo.y1 << "; " << patchZoneGeo.x2 << "; " << patchZoneGeo.y2 << ") " << patchCoord << endl;
					cout << " (x1;y1;x2;y2) = (" << patchZonePixel.x1 << "; " << patchZonePixel.y1 << "; " << patchZonePixel.x2 << "; " << patchZonePixel.y2 << ") pixels" << endl;
					cout << "Ground Truth :" << endl;
					cout << " (x1;y1;x2;y2) = (" << gtZoneGeo.x1 << "; " << gtZoneGeo.y1 << "; " << gtZoneGeo.x2 << "; " << gtZoneGeo.y2 << ") " << patchCoord << endl;
					cout << " (x1;y1;x2;y2) = (" << gtZonePixel.x1 << "; " << gtZonePixel.y1 << "; " << gtZonePixel.x2 << "; " << gtZonePixel.y2 << ") pixels" << endl;
					cout << endl;

					patchGTFile	= getPatchFile(band, "patchGT", patchX, patchY, gtZonePixel, mainGTClass, mainclassTh);
					patchGTPath	= patchGTDir+"/"+patchGTFile;
					cropImageUsingTriskele (gtPath, patchGTPath, 0, gtZoneGeo, gdalGtDataset);

					// ----------------------------------------------------------------------
					patchFile		= getPatchFile(band, "patch", patchX, patchY, patchZonePixel, mainGTClass, mainclassTh);
					patchPath		= patchDir+"/"+patchFile;
					cropImageUsingTriskele (imagePath, patchPath, band, patchZoneGeo, gdalImageDataset);
				}

				// ----------------------------------------------------------------------
				patch++;

			}			// for (patchX=0
		}			// for (patchY=0
	}			// for (band=0

	// finally remove temp file
	fs::remove(patchGTPathTmp);

	// NOTE: Press Ctrl + Z to stop the loop.

	// ----------------------------------------------------------------------
}

//// ======================================================================

void			Dataset::CreatePatches (string patchDir, SizePixel patchSizePixel, float patchOverlap, float mainclassTh)
{
	CRS imageCrs(gdalImageDataset);
	SizeGeo patchSizeGeo = imageCrs.pixelToGeo (patchSizePixel);
	CreatePatches (patchDir, patchSizeGeo, patchOverlap, mainclassTh);
}

// ======================================================================
// create a set of patch preview images (png) from inpimageages (tif)
// uses gdal_translate in command line

void			Dataset::CreatePatchesPreviews (string patchDir, string patchPreviewDir)
{
	string	patchExt 							= "tif";
	string	patchPreviewExt				= "png";
	int    	patchPreviewIntMin 		= 0;
	int			patchPreviewIntMax 		= 3000;
	string	patchPreviewIntMinStr	= to_string(patchPreviewIntMin);
	string	patchPreviewIntMaxStr	= to_string(patchPreviewIntMax);

	// if preview dir does not exist, create it
	if (!fs::exists(patchPreviewDir))
		fs::create_directory(fs::path(patchPreviewDir));

	// loop on patch images
	fs::path patchPath, patchFile, patchPreviewFile, patchPreviewPath;
	fs::path patchDirPath = fs::path(patchDir);
	int patch=0;
	for (fs::directory_iterator itr(patchDirPath); itr!=fs::directory_iterator(); ++itr)
		if ( boost::iequals ( fs::extension(itr->path()), "."+patchExt ) )
		{
			cout << "Patch " << patch++ << " :" << endl;
			// translate patch file (already exists) into patch preview file (to be created)
			patchPath				= itr->path();
			patchFile				= patchPath.filename();
			patchPreviewFile	= patchFile.replace_extension("."+patchPreviewExt);
			// operator "/" equals "/" for Linux and "\" for Windows
			patchPreviewPath	= fs::path(patchPreviewDir) / patchFile;

			// note: option "--config GDAL_PAM_ENABLED NO" disable the creation of .aux.xml files
			string bashCommand = "gdal_translate " + patchPath.string() + " " + patchPreviewPath.string() + " -of " + patchPreviewExt +
					" -ot Byte -scale " + patchPreviewIntMinStr + " " + patchPreviewIntMaxStr + " --config GDAL_PAM_ENABLED NO";
			cout << bashCommand << endl;
			system(("bash -c \"" + bashCommand + "\"").c_str());
			cout << endl;
		}

}

// ======================================================================
// Independent function to project one rectangular to another (using different EPSG)

ZoneGeo projectZoneGeo(ZoneGeo sourceZoneGeo, GDALDataset *gdalSourceDataset, GDALDataset *gdalTargetDataset)
{
  double zoneGeoX1, zoneGeoY1, zoneGeoX2, zoneGeoY2;


//	OGRSpatialReference sourceSpatialRef	= gdalSourceDataset->GetProjectionRef();
//  OGRSpatialReference targetSpatialRef	= gdalTargetDataset->GetProjectionRef();

//	OGRSpatialReference * sourceSpatialRef	= gdalSourceDataset->GetSpatialRef();
//	OGRSpatialReference * targetSpatialRef	= gdalTargetDataset->GetSpatialRef();
//	OGRCoordinateTransformation *poCT			= OGRCreateCoordinateTransformation (sourceSpatialRef, targetSpatialRef);

	// prepare the coordinate transformation object

  OGRSpatialReference sourceSpatialRef	= gdalDataset2SpatialRef (gdalSourceDataset);
  OGRSpatialReference targetSpatialRef	= gdalDataset2SpatialRef (gdalTargetDataset);
  OGRCoordinateTransformation *poCT			= OGRCreateCoordinateTransformation (&sourceSpatialRef, &targetSpatialRef);
  OGRLayer      *poLayer = nullptr;
  poLayer = gdalSourceDataset->GetLayer( 0 );

  // transform the coordinates
  if( poCT != NULL)
  {
		zoneGeoX1	= sourceZoneGeo.x1;
		zoneGeoY1	= sourceZoneGeo.y1;
		zoneGeoX2	= sourceZoneGeo.x2;
		zoneGeoY2	= sourceZoneGeo.y2;
		// note: the transform function operates in place
		!poCT->Transform( 1, &zoneGeoX1, &zoneGeoY1 );
		!poCT->Transform( 1, &zoneGeoX2, &zoneGeoY2 );
  }
  else
	{
  	cout << " *** coordinate transform: poCT == NULL ***";
		zoneGeoX1 = 0.0;
		zoneGeoY1 = 0.0;
		zoneGeoX2 = 0.0;
		zoneGeoY2 = 0.0;
	}
  ZoneGeo targetZoneGeo = ZoneGeo(zoneGeoX1, zoneGeoY1, zoneGeoX2, zoneGeoY2);
  return targetZoneGeo;
}

// ======================================================================
