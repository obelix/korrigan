////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iomanip>

#include "misc.hpp"
#include "KorriganStats.hpp"

using namespace obelix::korrigan;
using namespace std;
  
// ================================================================================
KorriganStats KorriganStats::global;

const string KorriganStats::timeTypeLabels [] = {
  "PS compute",
  "PS filter",
  "HDF5 save",
  "HDF5 load",

  "*** OUT OF TimeType ***"
};

// ================================================================================
KorriganStats::KorriganStats ()
  : timeStats (KTimeTypeCard) {
  reset ();
}

void
KorriganStats::reset () {
  for (unsigned int i = 0; i < KTimeTypeCard; ++i)
    timeStats[i] = KorriganStatsDouble ();
}

// ================================================================================
KorriganStats::CPrintTime::CPrintTime (const KorriganStats &korriganStats)
  : korriganStats (korriganStats) {
}

ostream &
KorriganStats::CPrintTime::print (ostream &out) const {
  bool empty = true;
  for (unsigned int i = 0; i < KTimeTypeCard; ++i)
    if (!(empty = !ba::count (korriganStats.timeStats[i])))
      break;
  if (empty)
    return out;
  out << endl
      << setw (16) << left << "Time"  << "\t"
      << setw (15) << left << "Sum"   << "\t"
      << setw (3)  << left << "Count" << "\t"
      << setw (15) << left << "Mean"  << "\t"
      << setw (15) << left << "Min"  << "\t"
      << setw (15) << left << "Max"  << endl;
  for (unsigned int i = 0; i < KTimeTypeCard; ++i) {
    if (!ba::count (korriganStats.timeStats[i]))
      continue;
    out << setw (16) << right << timeTypeLabels[i] << "\t"
	<< ns2string (ba::sum (korriganStats.timeStats[i]))  << "\t" << setw (3) << ba::count (korriganStats.timeStats[i]) << "\t"
	<< ns2string (ba::mean (korriganStats.timeStats[i])) << "\t"
	<< ns2string (ba::min (korriganStats.timeStats[i]))  << "\t"
	<< ns2string (ba::max (korriganStats.timeStats[i]))
	<< endl << flush;
  }
  return out;
}

KorriganStats::CPrintTime
KorriganStats::printTime () const {
  return CPrintTime (*this);
}

// ================================================================================
