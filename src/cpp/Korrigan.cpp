////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include<cmath> 

//for generator
#include<vector>
#include<algorithm>
#include<iterator>

#include "obelixDebug.hpp"
#include "Korrigan.hpp"

using namespace obelix;
using namespace obelix::korrigan;

const DimBin
obelix::korrigan::binRectCount (30);

const DimBin
obelix::korrigan::binAreaCount (30);


// ========================================
vector<double>
obelix::korrigan::logspace (const double &a, const double &b, const int &k) {
  DEF_LOG ("obelix::korrigan::logspace", "a: "<< a << " b: " << b << " k: " << k);
  vector<double> logspace;
  logspace.reserve (k);
  double exp_scale = (std::log (b) - std::log (a)) / (k - 1);
  for (int i = 0; i < k; ++i)
//    logspace.push_back (pow (2, std::log (a) + i * exp_scale));
    logspace.push_back ( exp (std::log (a) + i * exp_scale ) );
#ifndef DISABLE_LOG
  for (int i = 0; i < k; ++i)
    cerr << logspace [i] << " ";
  cerr << endl << flush;
#endif // !DISABLE_LOG
  return logspace;
}

// ========================================
vector<double>
obelix::korrigan::linearspace (const double &a, const double &b, const int &k) {
  DEF_LOG ("obelix::korrigan::linearspace", "a: "<< a << " b: " << b << " k: " << k);
  vector<double> linearspace;
  linearspace.reserve (k);
  for (int i = 0; i < k; ++i)
	  linearspace.push_back ( a + (b-a)*i/k );
#ifndef DISABLE_LOG
  for (int i = 0; i < k; ++i)
    cerr << linearspace [i] << " ";
  cerr << endl << flush;
#endif // !DISABLE_LOG
  return linearspace;
}

// ========================================

double
obelix::korrigan::distancePS (const vector<double> &ps1, const vector<double> &ps2) {
  double result (0);
  const DimBin reduceBinCount (ps1.size ());
  BOOST_ASSERT (reduceBinCount == ps2.size ());
  for (DimBin i (0); i < reduceBinCount; ++i) {
    const double diff (ps1[i]-ps2[i]);
    const double sum (ps1[i]+ps2[i]);
    result += (diff * diff) / sum;
  }
  return result;
}

// ========================================
GridConv::GridConv (const DimSideImg &overlap, const DimSideImg &patchSide)
  : overlap (overlap),
    patchSide (patchSide) {
}

// ========================================
Location::Location (double distance, DimSideImg x, DimSideImg y, DimSideImg width, DimSideImg height) :
  distance (distance),
  point (x, y),
  size (width, height) {
}

ostream &
obelix::korrigan::operator << (ostream &out, const Location &l) {
  return out << l.distance << "\t" << l.point.coord[0] << "," << l.point.coord[1] << "\t" << l.size.side[0] << "x" << l.size.side[1];
}

bool
obelix::korrigan::infLocation (const Location &a, const Location &b) {
  return (a.distance < b.distance);
}

// ========================================
