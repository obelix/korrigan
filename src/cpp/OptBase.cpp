////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#define LAST_VERSION "0.3 2020-01-07 ("+getOsName ()+")"

#include <iostream>
#include <string.h>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "misc.hpp"

#include "OptBase.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;
using namespace obelix::korrigan;



// ================================================================================
static po::command_line_parser foo (0, {});

OptBase::OptBase ()
  : mainDescription ("Main options", getCols ()),
    allDescription ("All options"),
    prog (nullptr),
    debugFlag (false),
    timeFlag (false),
    coreCount (boost::thread::hardware_concurrency ()),

    parsed (foo.run ()) {

  mainDescription.add_options ()
    ("help", po::bool_switch (&helpFlag), "produce this help message")
    ("version", po::bool_switch (&versionFlag), "display version information")
    ("debug", po::bool_switch (&debugFlag), "debug mode")
    ("timeFlag", po::bool_switch (&timeFlag), "give execution time")
    ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used to build tree")
    ;

  allDescription
    .add (mainDescription);
}

OptBase::OptBase (int argc, char** argv)
  : OptBase () {
  parse (argc, argv);
}

// ================================================================================
void
OptBase::extendedUsage () const {
}

void
OptBase::usage (string msg) const {
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [options] [-i] inputName [-o] outputName" << endl
       << endl << mainDescription;
    extendedUsage ();
    cout << endl;
  if (!msg.empty ())
    cout << msg << endl;
}

void
OptBase::version () const {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  GDAL  : read and write image (http://www.gdal.org/)" << endl
       << "  Boost : C++ libraries (http://www.boost.org/)" << endl
       << endl << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptBase::parse (int argc, char** argv) {
  prog = argv [0];
  allDescription.add_options ()
    (inputFileC, po::value<vector<string> > (), "input output")
    ;
  try {
    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    parsed = po::command_line_parser (argc, argv).options (allDescription).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (versionFlag)
      version ();
    if (helpFlag) {
      usage ();
      exit (0);
    }
    int required = 2;
    if (vm.count ("input"))
      required--;
    if (vm.count ("output"))
      required--;

    int nbArgs = 0;
    if (vm.count (inputFileC))
      nbArgs = vm[inputFileC].as<vector<string> > ().size ();
    if (required-nbArgs != 0) {
      usage ("Error: need one input and one output");
      exit (1);
    }
    if (required) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      if (outputName.empty ())
	outputName = var [--required];
      if (inputName.empty ())
	inputName = var [--required];
    }
    // XXX check outputName directory
  } catch (std::exception& e) {
    usage ("Bad options");
    cerr << "Error: " << e.what () << endl;
    exit (1);
  }
}

// ================================================================================
