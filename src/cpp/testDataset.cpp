////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * testDataset.cpp
 *
 *  Created on: 20 sept. 2019
 *      Author: more
 */

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
//#include <gdal_priv.h>

#include "cpl_port.h"
#include "gdal_priv.h"

#include "ogr_geometry.h"
#include "ogr_spatialref.h"

#include "dataset.hpp"

using namespace std;
namespace fs=boost::filesystem;

// ======================================================================

int main() {

	// ----------------------------------------------------------------------
	// user parameters

	string homeDir					= "/home/more";

	string imageDir					= homeDir+"/Images/dataset/dataset_in";
//	string imageFile				= "arles.tif";
//	string imageFile				= "IMG_L2A_TOA_20190214_a20190205T102913_pPHR1A_mMS.TIF";
//	string imageFile				= "IMG_SPOT6_MS_201305031037207_SEN_742626101_R1C1.JP2";	// Erreur de segmentation
//	string imageFile				= "IMG_L2A_TOA_20181025_a20181005T104652_pSPOT6_mMS.TIF";
//	string imageFile				= "IMG_L2A_TOA_20181025_a20181005T104652_pSPOT6_mP.TIF";	// Erreur de segmentation
//	string imageFile				= "IMG_L2A_TOA_20180420_a20140908T104458_pSPOT7_mMS.TIF";
	string imageFile				= "SENTINEL2A_20160929-102344-107_L2A_T32UMV_D_V1-1_ATB_R1.tif";
//	string imageFile				= "SENTINEL2A_20160929-102344-107_L2A_T32UMV_D_V1-1_FRE_B2.tif";
//	string imageFile				= "SPOT4_HRVIR_XS_20130402_N2A_AOT_CRennesD0000B0000.TIF";
//	string imageFile				= "SPOT4_HRVIR_XS_20130402_N2A_ORTHO_SURF_CORR_ENV_CRennesD0000B0000.TIF";
//	string imageFile				= "SPOT4_HRVIR_XS_20130402_N2A_ORTHO_SURF_CORR_PENTE_CRennesD0000B0000.TIF";

	string gtDir						= homeDir+"/Images/dataset/dataset_landcover";
	string gtFile						= "OCS_2017_CESBIO.tif";

	string patchDir					= homeDir+"/Images/dataset/dataset_out";
	string patchPreviewDir	= homeDir+"/Images/dataset/dataset_out_preview";

//	int patchWidth					= 800;
//	int patchHeight					= 600;
//	float patchOverlap			= 0.5;

	double patchWidthGeo		= 1600.;
	double patchHeightGeo		= 1200.;

	SizeGeo patchSizeGeo;
	patchSizeGeo.width			= patchWidthGeo;
	patchSizeGeo.height			= patchHeightGeo;

	float patchOverlap			= 0.5;
	float mainclassTh				= 0.9;

//	int patchWidth					= 8000;
//	int patchHeight					= 6000;
//	float patchOverlap			= 0.25;
//	string patchCoord				="pixels";

	// ----------------------------------------------------------------------

	cout << endl;
	cout << "--------------------------------------------" << endl;
	cout << "- testDataset.cpp -" << endl;
	cout << "--------------------------------------------" << endl;
	cout << endl;

	// ----------------------------------------------------------------------

	Dataset dataset;
	dataset.Init (imageDir, imageFile, gtDir, gtFile);
	dataset.CreatePatches (patchDir, patchSizeGeo, patchOverlap, mainclassTh);
	dataset.CreatePatchesPreviews (patchDir, patchPreviewDir);

	// ----------------------------------------------------------------------

}

// ======================================================================
