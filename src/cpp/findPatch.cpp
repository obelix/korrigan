////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

// ./findPatch --timeFlag ../result/arles_1576x920-1000x1000-b0_100x100-200x200.patch ../result/arles_1576x920-1000x1000-b0.kor --bin 96% --overlap 4 --findLimit 10

#include <iostream>
#include <boost/filesystem.hpp>
#include <algorithm>    // std::binary_search, std::sort

#include "obelixDebug.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"

#include "Korrigan.hpp"
#include "KorriganStats.hpp"
#include "OptFind.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace obelix;
using namespace obelix::triskele;
using namespace obelix::korrigan;

bool
nullValIn (const vector<double> &ps, size_t maxNull) {
  for (DimBin i = 0; i < ps.size (); ++i)
    if (! ps[i] && !maxNull--)
      return true;
  return false;
}

void
findPatch (const string &patchInputFileName, const string &korInputFileName, const OptFind &optFind) {
  DEF_LOG ("findPatch", "");

  vector<DimSideImg> patchSize;
  vector<DimBin> reduceBinIdx;
  vector<double> reduceBinVal;

  HDF5 hdf5;
  hdf5.open_read (patchInputFileName);
  hdf5.read_array (patchSize, "patchSize", "/");
  hdf5.read_array (reduceBinIdx, "patchPSBinIdx", "/" );
  hdf5.read_array (reduceBinVal, "patchPSBinVal", "/");
  hdf5.close_file ();

  const DimSideImg
    patchOverlap (optFind.overlap),
    patchWidth (patchSize [0]),
    patchHeight (patchSize [1]);
  const GridConv
    xConv (patchOverlap, patchWidth),
    yConv (patchOverlap, patchHeight);

  LOG ("patchSize:" << patchSize[0] << "x" << patchSize[1] << " overlap:" << patchOverlap << " bin size:" << reduceBinIdx.size ());

  Tree<2> tree (optFind.coreCount);
  vector<double> psTab;
  vector<DimBin> compBinIdx;
  vector<double> compPSVal;
  
  hdf5.open_read (korInputFileName);
  tree.load (hdf5);
  hdf5.read_array (psTab, "PS", "/");
  hdf5.read_array (compBinIdx, "compBinIdx", "/");
  hdf5.read_array (compPSVal, "compBinVal", "/");
  hdf5.close_file ();

  const DimSideImg
    imgWidth (tree.getSize ().side[0]),
    imgHeight (tree.getSize ().side[1]);
  
  LOG ("imageSize:" << imgWidth << "x" << imgHeight << " comp size:" << compBinIdx.size ());

  const BoundingBoxAttributes<2> boundingBoxAttributes (tree);
  vector<double> patchPSVal (psTab.size ());
  const BoundingBox<2> *bbValues = boundingBoxAttributes.getValues ();

  const DimSideImg nbCols (xConv.getGrid (imgWidth) + patchOverlap);
  const DimSideImg nbRow (yConv.getGrid (imgHeight) + patchOverlap);
  LOG ("nbRow:" << nbRow << " nbCols:" << nbCols << " reduceBinIdx: " << reduceBinIdx.size ());
  
  vector<vector<vector<double> > > gridPS (nbCols, vector<vector<double> > (nbRow, vector<double> (reduceBinIdx.size (), 0)));

  const DimParent rootId (tree.getRootId ());
  KorriganStats::global.setStart ();

  // XXX check dealThread ok (concurrent access to gridPS [i][j][rIdx] += compPSVal [compId])
  for (DimParent compId = 0; compId < rootId; ++compId) {
  // dealThread (rootId, optFind.coreCount,
  // 	      [&reduceBinIdx, & compBinIdx, &bbValues, &xConv, &yConv, &patchOverlap, &gridPS, &compPSVal]
  // 	      (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
  // 		for (DimParent compId = minVal; compId < maxVal; ++compId) {
		  vector<DimBin>::iterator low = lower_bound (reduceBinIdx.begin (), reduceBinIdx.end (), compBinIdx[compId]);
		  if (*low != compBinIdx[compId])
		    continue;
		  const BoundingBox<2> &bbc (bbValues[compId]);
		  const DimSideImg
		    xGridMin (xConv.getGrid (bbc.max[0]-1)),
		    yGridMin (yConv.getGrid (bbc.max[1]-1)),
		    xGridMax (xConv.getGrid (bbc.min[0]) + patchOverlap),
		    yGridMax (yConv.getGrid (bbc.min[1]) + patchOverlap);
		  DimBin rIdx (low-reduceBinIdx.begin ());
		  for (DimSideImg i (xGridMin); i < xGridMax; ++i)
		    for (DimSideImg j (yGridMin); j < yGridMax; ++j)
		      gridPS [i][j][rIdx] += compPSVal [compId];
		}
	      // });
  KorriganStats::global.addTime (PSfilter);
		
  vector<Location> list;

  const DimSideImg overlapMinus1 (patchOverlap -1);
  size_t maxNull = reduceBinIdx.size () - optFind.binRate.getRate (reduceBinIdx.size ());
  for (DimSideImg i = 0; i < nbCols; ++i)
    for (DimSideImg j = 0; j < nbRow; ++j) {
      if (nullValIn (gridPS [i][j], maxNull))
	continue;
      const double d = distancePS (reduceBinVal, gridPS [i][j]);
      const DimSideImg
	x (xConv.getPixel (i < patchOverlap ? 0 : i-overlapMinus1)),
	y (yConv.getPixel (j < patchOverlap ? 0 : j-overlapMinus1)),
	w (i < patchOverlap ? xConv.getPixel (i+1) : min (patchWidth, imgWidth-x)),
	h (j < patchOverlap ? xConv.getPixel (j+1) : min (patchHeight, imgHeight-y));
      list.push_back (Location (d, x, y, w, h));
    }

  cout << "find " << list.size () << " locations (on " << (DimImg (nbRow)*nbCols*patchOverlap*patchOverlap)<< " places)" << endl;
  sort (list.begin (), list.end (), infLocation);

  DimImg locationCount (0);
  const DimImg findLimit (optFind.findLimit);
  for (const Location &l : list) {
    cout << l << endl;
    //cout << xConv.getGrid (l.point.x) << " " << xConv.getPixel (xConv.getGrid (l.point.x)) << endl;
    if (++locationCount == findLimit)
      break;
  }
}

int
main (int argc, char** argv, char** envp) {
  OptFind optFind (argc, argv);

  cout << "findPatch input" << endl
       << "           patch: " << optFind.inputName << endl
       << " image candidate: " << optFind.outputName << endl
       << endl
       << "findPatch output" << endl
       << "        location: stdout" << endl;

  findPatch (optFind.inputName, optFind.outputName, optFind);
  if (optFind.timeFlag) {
    cerr << TreeStats::global.printTime ();
    cerr << KorriganStats::global.printTime ();
  }
  return 0;
}
