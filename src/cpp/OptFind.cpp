////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This software is  a computer program whose purpose is  to search patch //
// in remote sensing image based on pattern spectra.			  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string.h>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "misc.hpp"
#include "Appli/OptCropParser.hpp"

#include "OptFind.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;
using namespace obelix::korrigan;

namespace po = boost::program_options;

// ================================================================================
static OptCropParser optCropParser;
OptFind::OptFind ()
  : OptBase (),
    binRate ("100%"),
    overlap (4),
    findLimit (0) {

  findDescription.add_options ()
    ("binRate", po::value<OptRate> (&binRate)->default_value (binRate), "not null bin rate in patch")
    ("overlap", po::value<int> (&overlap)->default_value (overlap), "horizontal and vertical sliding window overlap")
    ("findLimit", po::value<DimImg> (&findLimit)->default_value (findLimit), "max number of matched location (0 = no limit)")
    ;

  allDescription
    .add (findDescription);
}

OptFind::OptFind (int argc, char** argv)
  : OptFind () {
  parse (argc, argv);
}

// ================================================================================
void
OptFind::extendedUsage () const {
  cout << findDescription;
}

// void
// OptFind::parse (int argc, char** argv) {
//   OptBase::parse (argc, argv);
// }
