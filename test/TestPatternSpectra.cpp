#include <iostream>
#include <boost/filesystem.hpp>
#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
//#include "AttributeProfiles.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "PatternSpectra.hpp"
#include "gdal.h"

using namespace obelix;
using namespace obelix::triskele;

inline unsigned int abs (unsigned int x) { return x;}

template<typename InPixelT>
void
patternSpectra (IImage<2> &inputImage) {

  TreeType treeType = MAX;

  Raster<InPixelT, 2> raster;
  inputImage.readBand (raster, 0, NullPoint2D, inputImage.getSize ());

  Border<2> border (inputImage.getSize (), false);
  GraphWalker<2> graphWalker (border);
  ArrayTreeBuilder<InPixelT, InPixelT, 2> atb (raster, graphWalker, treeType);
  const DimCore coreCount (boost::thread::hardware_concurrency ());
  Tree<2> tree (coreCount);
  WeightAttributes<InPixelT, 2> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);

  //AttributeProfiles<OutPixelT> attributeProfiles (tree);
  AreaAttributes<2> areaAttributes (tree);

  DimImg pixelCount = inputImage.getSize ().getPixelsCount ();

  int binCount = 40;
  //vector<InPixelT> psTab (binCount, 0);
  PatternSpectra psTab(binCount, 1, 0, 0, 0, 0, inputImage.getSize().side[0], inputImage.getSize().side[1], inputImage.getFileName());
  for (DimImg nodeId = 0; nodeId < tree.getRootId (); ++nodeId){
    //psTab [int (areaAttributes.getValues ()[nodeId] * binCount / pixelCount)]
    //  += abs (weightAttributes.getValues ()[nodeId]-weightAttributes.getValues ()[tree.getCompParent (nodeId)])*
    //  areaAttributes.getValues ()[nodeId];
    int i1= int (areaAttributes.getValues ()[nodeId] * binCount / pixelCount);
    uint32_t v= abs (
        weightAttributes.getValues ()[nodeId]-
        weightAttributes.getValues ()[tree.getCompParent (nodeId)])
            *areaAttributes.getValues ()[nodeId];
    //cout << "bins[" << i1 << "] += " << v << endl;
    psTab.add_count(v,i1,0);
  }
  cout << "before write psTab=\n";
  for (int binId = 0; binId < binCount; ++binId)
    //cout << " " << psTab[binId];
    cout << " " << psTab.get_count(binId,0);
  cout << endl;
  cout << "open_write returns " << psTab.open_write_h5("psTab.h5") << endl;
  cout << "store_to_h5 returns " << psTab.store_to_h5() << endl;
  cout << "close_h5 returns " << psTab.close_h5() << endl;
  cout << "open_read_h5 returns " << psTab.open_read_h5("psTab.h5") << endl;
  cout << "load_from_h5 returns " << psTab.load_from_h5() << endl;
  cout << "close_h5 returns " << psTab.close_h5() << endl;
  cout << "after write psTab=\n";
  for (int binId = 0; binId < binCount; ++binId)
      //cout << " " << psTab[binId];
      cout << " " << psTab.get_count(binId,0);
    cout << endl;

  // XXX write result
}


int
main (int argc, char** argv, char** envp) {
  IImage<2> inputImage ("data/arles.tif");

  inputImage.readImage ();

  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    patternSpectra<uint8_t> (inputImage); break;
  case GDT_UInt16:
    patternSpectra<uint16_t> (inputImage); break;
  case GDT_Int16:
    patternSpectra<int16_t> (inputImage); break;
  case GDT_UInt32:
    patternSpectra<uint32_t> (inputImage); break;
  case GDT_Int32:
    patternSpectra<int32_t> (inputImage); break;
  case GDT_Float32:
    patternSpectra<float> (inputImage); break;
  case GDT_Float64:
    patternSpectra<double> (inputImage); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;

}
