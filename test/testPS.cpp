#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"
#include "Attributes/RectangularityAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;

template<typename InPixelT>
void
patternSpectra (IImage<2> &inputImage) {

  TreeType treeType = MAX;

  Raster<InPixelT, 2> raster;
  inputImage.readBand (raster, 0, NullPoint2D, inputImage.getSize ());

  Border<2> border (inputImage.getSize (), false);
  GraphWalker<2> graphWalker (border);
  ArrayTreeBuilder<InPixelT, InPixelT, 2> atb (raster, graphWalker, treeType);
  const DimCore coreCount (boost::thread::hardware_concurrency ());
  Tree<2> tree (coreCount);
  WeightAttributes<InPixelT, 2> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);

  AreaAttributes<2> areaAttributes (tree);
  BoundingBoxAttributes<2> boundingBoxAttributes (tree);
  RectangularityAttributes<2> rectangularityAttributes (tree, areaAttributes, boundingBoxAttributes);

  const DimImg pixelCount = inputImage.getSize ().getPixelsCount ();
  const DimImg *areaValues = areaAttributes.getValues ();
  const InPixelT *weightValues = weightAttributes.getValues ();
  const double *rectValues = rectangularityAttributes.getValues ();

  int binRectCount (30), binAreaCount (30);
  vector<vector<double> > psTab (binAreaCount, vector<double> (binRectCount, 0));

  for (DimImg nodeId = 0; nodeId < tree.getRootId (); ++nodeId) {

    int areaBinId = areaValues[nodeId] * binAreaCount / pixelCount;
    int rectBinId = rectValues[nodeId] * binRectCount / 1.;

    double diff = abs ((double)weightValues[nodeId]-
		       (double)weightValues[tree.getCompParent (nodeId)]);
    double volume = diff*areaValues[nodeId];
    
    psTab [areaBinId][rectBinId] += volume;
  }

  for (int areaBinId = 0; areaBinId < binAreaCount; ++areaBinId) {
    for (int rectBinId = 0; rectBinId < binRectCount; ++rectBinId)
      cout << " " << psTab[areaBinId][rectBinId];
    cout << endl;
  }
  cout << endl;

  // XXX write result
}


int
main (int argc, char** argv, char** envp) {
  if (argc != 2) {
    cout << "Usage: " << argv[0] << " imageFileName" << endl;
    exit (0);
  }

  IImage<2> inputImage (argv[1]);

  inputImage.readImage ();

  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    patternSpectra<uint8_t> (inputImage); break;
  case GDT_UInt16:
    patternSpectra<uint16_t> (inputImage); break;
  case GDT_Int16:
    patternSpectra<int16_t> (inputImage); break;
  case GDT_UInt32:
    patternSpectra<uint32_t> (inputImage); break;
  case GDT_Int32:
    patternSpectra<int32_t> (inputImage); break;
  case GDT_Float32:
    patternSpectra<float> (inputImage); break;
  case GDT_Float64:
    patternSpectra<double> (inputImage); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}
