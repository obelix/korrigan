 Korrigan

Korrigan aims to use TRISKELE for seaking patch on database image.


## Compilation

```bash

sudo apt-get install --fix-missing build-essential git cmake g++ libboost-system-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-filesystem-dev libboost-date-time-dev libboost-serialization-dev libboost-thread-dev libhdf5-dev python3-h5py gdal-bin libgdal-dev libtbb-dev libtinyxml-dev

apt-get install python3-h5py
apt-get install hdf5-tools

git clone https://gitlab.kalimsat.eu/studies/korrigan.git

cd korrigan ; git submodule init && git submodule update

cd thirdparty/triskele ; git checkout master && git fetch -a && git pull && git submodule init && git submodule update ; cd ../..

mkdir ../build ; cd ../build

#cmake -DCMAKE_BUILD_TYPE=Debug ../korrigan -G"Eclipse CDT4 - Unix Makefiles"
cmake -DCMAKE_BUILD_TYPE=Release ../korrigan

make -j $(nproc)
```
